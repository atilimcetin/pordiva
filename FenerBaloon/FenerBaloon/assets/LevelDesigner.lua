LevelDesigner = gideros.class(Sprite)

bound_X0 = 30
bound_XH = 640 - 30



Balon00 = {}

for i =1, 16 do		
	Balon00[i] = {}
	for j = 1,16 do
		Balon00[i][j] = { type = 0,
						on = true}
	end 
end 


function LevelDesigner:init()
	self.ballCounter = 1
	self.qSprite = Sprite.new()
	self:loadBK()
	--self:drawGrid()
	self:loadLevel(1)
	self.name = "Level["
	self.levelNo = 150
	negative = true
	self:addEventListener(Event.MOUSE_UP, self.onMouseUp, self)

end 	


	
function LevelDesigner:onMouseUp(event)

	local j,i = pixel_to_grid(event.x, event.y)
	printz(i,j)
	if i>0 and i <=16 and j>0 and j<=10 then 
	
		Balon00[i][j].type = Balon00[i][j].type + 1
		if Balon00[i][j].type > 10 then Balon00[i][j].type = 0 end 
		self.qSprite:removeChild(Balon00[i][j].image)
		Balon00[i][j].image = Bitmap.new(TextureRegion.new(Texture.new(balls[Balon00[i][j].type], true )))
		Balon00[i][j].image:setAnchorPoint(0.5,0.5)
		Balon00[i][j].image:setPosition(Balon00[i][j].xx, Balon00[i][j].yy)
		Balon00[i][j].type = Balon00[i][j].type
		printz(i,j,Balon00[i][j].type )
		self.qSprite:addChild(Balon00[i][j].image)
	else
			printa("---", i,j)
		for y = 1,16 do	
			for x = 1,10 do
				--printz("---",y,x,Balon00[y][x].type, negative)	
				if Balon00[y][x].type > 0 then 
					if negative  then 
						printx(self.name,self.levelNo,"][",y-16,"][",x,"] = { type = " , Balon00[y][x].type, ",  on = true }" )
					else
						printx(self.name,self.levelNo,"][",y,"][",x,"] = { type = " , Balon00[y][x].type, ",  on = true }" )
					end 
				end 
			end 
		end 
	
	end 	
end 





function LevelDesigner:loadBK()
	self.qSprite.bk = Bitmap.new(TextureRegion.new(Texture.new("gfx/back.png", true )))
	self.qSprite.bk:setAlpha(0.5)
	self.qSprite:addChild(self.qSprite.bk)
	local xx = (X_HIGH - self.qSprite.bk:getWidth())  / 2
	local yy = (Y_HIGH - self.qSprite.bk:getHeight())  / 2
	self.qSprite.bk:setPosition(xx,yy)
	self.xx = xx
	self.yy = yy
	self:addChild(self.qSprite)

end 

function LevelDesigner:loadLevel(levelno)

	for i = 1,#Balon00 do
		temp = Balon00[i]
		for j = 1, #temp do 
			local p = i%2  
			local item = Balon00[i][j]
				
			if p == 1 then 
				xx = j* 60 
				yy = yylist[i]
			end 
			if p == 0 then 
				xx = j* 60 -30
				yy = yylist[i]
			end 		
			
			Balon00[i][j].xx = xx
			Balon00[i][j].yy = yy
			Balon00[i][j].dirty = false 
			
			if temp[j].on then  
				local randEff = Balon00[i][j].type
				
				Balon00[i][j].image = Bitmap.new(TextureRegion.new(Texture.new(balls[randEff], true )))
				Balon00[i][j].type = randEff

				Balon00[i][j].image:setAnchorPoint(0.5,0.5)

				Balon00[i][j].image:setPosition(xx,yy)	
				self.qSprite:addChild(Balon00[i][j].image)
			end 	
		end 
	end 	
	--self.qSprite:setPosition(10,20)

end 

