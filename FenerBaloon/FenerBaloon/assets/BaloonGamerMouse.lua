function BaloonGame:onMouseDown(event)
	if self.gameEnd == false and self.mouseOn then 
		if self:checkDots(event.x, event.y) then 
			self:putDots(event.x, event.y-40, false)
		else
			self:putDots(event.x, event.y-40, true)
		end 
	end 	
end 

function BaloonGame:checkDots(x,y)
	x = x - self.origin.x
	y = y - self.origin.y
	local degree = math.deg(math.atan2(x,y))
	printa(degree)
	degree = math.abs(degree)
	if degree < 117 then return false end 
	if degree > 180 then return false end 
	return true 
	
end 

function BaloonGame:onMouseMove(event)
	if self.gameEnd == false and self.mouseOn then 
		if self:checkDots(event.x, event.y-40) then 
			self:putDots(event.x, event.y-40, false)
		else
			self:putDots(event.x, event.y-40, true)
		end 
	end 
end 

function BaloonGame:onMouseUp(event)
	for i=1,20 do
		if self.qSprite:contains(self.dotList[i]) then 
			self.qSprite:removeChild(self.dotList[i])
		end	
	end 

	printz(" - - MOUSE UP - - ")
	if self.gameEnd == false then 
		if self.mouseOn then 
			if self:checkDots(event.x, event.y-40) then 
				self:manAnimation()
				
				
				xx,yy  = self.qSprite:getPosition()
			
				self.targetPoint = {x = event.x , y  = event.y -40}
				print(xx,yy,event.x, event.y, self.targetPoint.x, self.targetPoint.y)
				self:calculateDirection(self.targetPoint)
				self.mouseOn = false 
			end 	
			--self:testing()
		end 	
	else
		if self.failing then 
				if self.subScreen2:hitTestPoint(event.x, event.y) == true then 
					ev= Event.new("hebe")
					ev.oldlevel = self.levelNo - 100
					ev.page = 2
					ev.level = self.levelNo - 100
					self.manager:gotoPage(ev)	
				end 	
		else 
		
			if self.subScreen:hitTestPoint(event.x, event.y) == true then 
				ev= Event.new("hebe")
				ev.oldlevel = self.levelNo - 100
				ev.page = 2
				ev.level = self.levelNo + 1 - 100
				self.manager:gotoPage(ev)	
			end 
		end 
	
	end 
end 
