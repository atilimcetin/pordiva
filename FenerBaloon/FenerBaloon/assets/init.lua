printxx = print
print = function() end

printz = function() end 
printa = function() end 
printx = function() end 

gameVolume = 1

function axial_to_odd_r(x, z) 
    local q = x + (z - math.abs(z%2)) / 2
    local r = z
    return q,r
end

function odd_r_to_axial(q, r) 
    local x = q - (r - math.abs(r%2)) / 2
    local z = r
    return x,z
end

function axial_round(x, z)
    local y = -x - z

    local rx = math.floor(x + 0.5)
    local ry = math.floor(y + 0.5)
    local rz = math.floor(z + 0.5)
    
    local x_diff = math.abs(rx - x)
    local y_diff = math.abs(ry - y)
    local z_diff = math.abs(rz - z)

    if x_diff > y_diff and x_diff > z_diff then
        rx = -ry-rz
    elseif y_diff > z_diff then
        ry = -rx-rz
    else
        rz = -rx-ry
    end

    return rx, rz
end

function axial_to_pixel(size, q, r) 
    local x = size * math.sqrt(3) * (q + r/2)
    local y = size * 3/2 * r
    return x, y
end

function pixel_to_axial(size, x, y)
    local q = (1/3*math.sqrt(3) * x - 1/3 * y) / size
    local r = 2/3 * y / size
    return q, r
end


function axial_to_pixel_modified(size, q, r)
    local c = 1 / math.sqrt(3)

    local x,y = axial_to_pixel(size * c, q, r)

    x = x - size / 2
    y = y - size * c * 3/2 + size / 2

    return x, y
end


function pixel_to_axial_modified(size, x, y)
    local c = 1 / math.sqrt(3)

    x = x + size / 2
    y = y + size * c * 3/2 - size / 2

    local q, r = pixel_to_axial(size * c, x, y)

    return q, r
end

function pixel_to_grid(x, y)
    return axial_to_odd_r(axial_round(pixel_to_axial_modified(60, x, y)))
end

function grid_to_pixel(q, r)
    return axial_to_pixel_modified(60, odd_r_to_axial(q, r))
end