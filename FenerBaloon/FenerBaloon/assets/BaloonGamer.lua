BaloonGame = gideros.class(Sprite)


bound_XH = 640 - 30

increment = 15

minYindex = -31
maxYindex = 18

ff = Font.new("font/c.txt", "font/c.png")

fff = TTFont.new("font/Roboto-Regular.ttf", 60, "1234567890-", true)

cheerSounds = {}
cheerSounds[1] = "sound/cheer1.wav"
cheerSounds[2] = "sound/cheer2.wav"
cheerSounds[3] = "sound/cheer3.wav"
cheerSounds[4] = "sound/cheer4.wav"


function BaloonGame:onEnterFrame(event)

	if self.movingBall then 
	
		printz("-----------------------------------------------" )	
		self.currentBaloon.x = self.currentBaloon.x + self.direction.x * increment
		self.currentBaloon.y = self.currentBaloon.y + self.direction.y * increment
		if self.currentBaloon.x > bound_XH or self.currentBaloon.x < bound_X0 then 
			self.currentBaloon.x = self.currentBaloon.x - self.direction.x * increment * 2
			self.direction.x = -self.direction.x
		end 
		
		--self.currentBaloon:setX(math.floor(self.currentBaloon.x))
		--self.currentBaloon:setY(math.floor(self.currentBaloon.y))
		for i=1,1 do
			self.currentBaloons[i]:setPosition(self.currentBaloon.x, self.currentBaloon.y)
			--self.currentBaloons[i]:setVisible(false)
		end 
		self.counter = self.counter + 0.2
		
	--	self.currentBaloons[math.floor(self.counter)%3 + 1]:setVisible(true)
	
		self:calculateHit()	
	end 
	

	
end 

function BaloonGame:init(levelNo)
	printxx("baloongameloading -- ", "init")
	printz(" - - INIT - - ")
	bound_X0 = 5
	printz("-----------------------------------------------" , bound_X0)	
	self.ballCounter = 1
	
	self.maxBallCounter = Level[levelNo].maxBalls
	self.qSprite = Sprite.new()
	self.levelNo = levelNo 
	Level[self.levelNo].puan =  0
	
	self.tempPuan = 0 
	self.oldSprites = {}
	self.movingBall = false 
	self.successState = 0 
	
	self.imagem1 = Sprite.new()
	self.imagem2 = Sprite.new()
	
	if levelNo == nil  then print("LEVEL no nil geliyor") end 
	Balon = {}
	for i = -16, 18 do
		Balon[i] = {}
		for j = 1,11 do
			Balon[i][j] = {}
			if Level[levelNo][i][j] ~= nil then 
				Balon[i][j].type = Level[levelNo][i][j].type 
				Balon[i][j].on = Level[levelNo][i][j].on 
			--	printa("hadi ya", i,j, Balon[i][j].type, Balon[i][j].on)
			end 
		end 
	end 
	Balon.up = Level[levelNo].up
	Balon.down = Level[levelNo].down
	Balon.maxBalls = Level[levelNo].maxBalls 
	
	
	
--	print("balon", Balon, Level[levelNo], levelNo, #Level)
	self:loadBK()
	--self:drawGrid()	
	self:loadLevel(levelNo)
	
	
	self.origin = {x = self.qSprite:getWidth()/2, y = Y_HIGH - 200}		
--	print("coords", self.origin.x, self.origin.y, X_HIGH /2, Y_HIGH /2)
	self.originX = {x = self.qSprite:getWidth()/2, y = Y_HIGH - 200  }	
	
	self.origin = {x = X_HIGH/2, y = Y_HIGH - 200}		
	self.originX = {x = X_HIGH/2, y = Y_HIGH - 200  }		
	
	self:newBall()
	self:startLoadAnimation()
	
	self.start = {}	
	

	self.emptyNo = 0
	self.downLimit = 16
	
	self.gameEnd = false

	self.counter = 1
end 	



function BaloonGame:putDots(x,y, delete)
		printxx("baloongameloading -- ", "putDots")
	for i=1,20 do
		if self.qSprite:contains(self.dotList[i]) then 
			self.qSprite:removeChild(self.dotList[i])
		end	
	end 
	
	if delete then return end 
	
	
	self.directionDots = {x = x - self.origin.x  , y = y - self.origin.y}
	local total = self.directionDots.x * self.directionDots.x + self.directionDots.y * self.directionDots.y
	total = math.sqrt(total)
	self.directionDots.x  = self.directionDots.x / total
	self.directionDots.y  = self.directionDots.y / total
	
	local xx  = self.origin.x + self.directionDots.x  * 10
	local yy  = self.origin.y + self.directionDots.y  * 10
	local count = 1
	while xx > 0  and xx < X_HIGH and count < 10 do  
		self.dotList[count]:setPosition(xx,yy )
		self.dotList[count]:setAnchorPoint(0.5, 0.5)
		xx  = xx + self.directionDots.x  * 40
		yy  = yy + self.directionDots.y  * 40	
		self.qSprite:addChild(self.dotList[count])
			
		count = count  + 1
	end 
end 




function BaloonGame:puanAnimation1(puan,x, y )
	printxx("baloongameloading -- ", "puanAnimation1")
	ColorNumbers = YellowNumbers

	self:puanAnimation(puan,x,y, self.imagem1 )
end 

function BaloonGame:puanAnimation2(puan ,x, y)
	printxx("baloongameloading -- ", "puanAnimation2")
	ColorNumbers = PurpleNumbers
	self:puanAnimation(puan,x,y, self.imagem2  )
end 

function BaloonGame:puanAnimation(puan,x, y, pImage )
	printxx("baloongameloading -- ", "puanAnimation")
	if puan < 1 then return end 

	if pImage then 

		if self.qSprite:contains(pImage) then 
			self.qSprite:removeChild(pImage)
		end 
	end 
	
	pImage = Sprite.new()

	
	if puan < 0  then puan = -1 * puan end
    local n1 = math.floor(puan%10)
	local n10 = math.floor((puan/10)%10)
	local n100 = math.floor((puan/100)%10)
	local n1000 = math.floor((puan/1000)%10)
	local n10000 = math.floor((puan/10000)%10)
	--printa("                      THE NUMBER   :", puan, "-------->>>>",n10000, n1000, n100, n10, n1, "-- -- -- -- -- x :", x, "y :", y )
	
	if n10000 > 0 then 
		local im1 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n10000], true )))
		local im2 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n1000], true )))
		local im3 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n100], true )))
		local im4 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n10], true )))
		local im5 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n1], true )))
		pImage:addChild(im1)
		pImage:addChild(im2)
		pImage:addChild(im3)
		pImage:addChild(im4)
		pImage:addChild(im5)

		im1:setPosition(0,0)
		im2:setPosition(im1:getX() + im2:getWidth(), 0)
		im3:setPosition(im2:getX() + im3:getWidth(), 0)
		im4:setPosition(im3:getX() + im4:getWidth(), 0)
		im5:setPosition(im4:getX() + im5:getWidth(), 0)
	elseif n1000 > 0 then 
		local im1 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n1000], true )))
		local im2 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n100], true )))
		local im3 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n10], true )))
		local im4 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n1], true )))

		pImage:addChild(im1)
		pImage:addChild(im2)
		pImage:addChild(im3)
		pImage:addChild(im4)

		im1:setPosition(0,0)
		im2:setPosition(im1:getX() + im2:getWidth(), 0)
		im3:setPosition(im2:getX() + im3:getWidth(), 0)
		im4:setPosition(im3:getX() + im4:getWidth(), 0)

	elseif n100 > 0 then 
		local im1 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n100], true )))
		local im2 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n10], true )))
		local im3 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n1], true )))
		pImage:addChild(im1)
		pImage:addChild(im2)
		pImage:addChild(im3)

		im1:setPosition(0,0)
		im2:setPosition(im1:getX() + im2:getWidth(), 0)
		im3:setPosition(im2:getX() + im3:getWidth(), 0)
	
	elseif n10 > 0 then 
		local im1 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n10], true )))
		local im2 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n1], true )))

		pImage:addChild(im1)
		pImage:addChild(im2)

		im1:setPosition(0,0)
		im2:setPosition(im1:getX() + im2:getWidth(), 0)

	else
		local im1 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n1], true )))
		pImage:addChild(im1)
		im1:setPosition(0,0)
	end 
	
	self.qSprite:addChild(pImage)
	pImage:setPosition(x,y) 
	
	GTween.new(pImage, 2, {alpha = 0, y = y-200, scaleX = 3, scaleY = 6}, { ease = easing.linear, delay =0, repeatCount = 1, reflect = false } )				
		
	
	self:getTotalScore()

end 

function BaloonGame:getTotalScore( )
	printxx("baloongameloading -- ", "getTotalScore")
	if self.scoreImage then 
		if self:contains(self.scoreImage) then 
			self:removeChild(self.scoreImage)
		end 
	end 	
	self.scoreImage = Sprite.new() 
	pImage = self.scoreImage 
	
	local num =self.scoreImage:getNumChildren()
	for i = num, 1, -1 do
		self.scoreImage:removeChildAt(i)
	end 
	
	
	ColorNumbers = GreenNumbers 
	puan = Level[self.levelNo].puan
	
	if pImage then 
		if self.qSprite:contains(pImage) then 
			self.qSprite:removeChild(pImage)
		end 
	end 

	if puan < 0  then puan = -1 * puan end
    local n1 = math.floor(puan%10)
	local n10 = math.floor((puan/10)%10)
	local n100 = math.floor((puan/100)%10)
	local n1000 = math.floor((puan/1000)%10)
	local n10000 = math.floor((puan/10000)%10)
	local n100000 = math.floor((puan/100000)%10)

	if n100000 > 0 then 
		local im0 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n100000], true )))
		local im1 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n10000], true )))
		local im2 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n1000], true )))
		local im3 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n100], true )))
		local im4 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n10], true )))
		local im5 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n1], true )))
		pImage:addChild(im0)
		pImage:addChild(im1)
		pImage:addChild(im2)
		pImage:addChild(im3)
		pImage:addChild(im4)
		pImage:addChild(im5)

		im0:setPosition(0,0)
		im1:setPosition(im0:getX() + im1:getWidth(), 0)
		im2:setPosition(im1:getX() + im2:getWidth(), 0)
		im3:setPosition(im2:getX() + im3:getWidth(), 0)
		im4:setPosition(im3:getX() + im4:getWidth(), 0)
		im5:setPosition(im4:getX() + im5:getWidth(), 0)
		
	elseif n10000 > 0 then 
		local im1 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n10000], true )))
		local im2 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n1000], true )))
		local im3 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n100], true )))
		local im4 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n10], true )))
		local im5 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n1], true )))
		pImage:addChild(im1)
		pImage:addChild(im2)
		pImage:addChild(im3)
		pImage:addChild(im4)
		pImage:addChild(im5)

		im1:setPosition(0,0)
		im2:setPosition(im1:getX() + im2:getWidth(), 0)
		im3:setPosition(im2:getX() + im3:getWidth(), 0)
		im4:setPosition(im3:getX() + im4:getWidth(), 0)
		im5:setPosition(im4:getX() + im5:getWidth(), 0)
	elseif n1000 > 0 then 
		local im1 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n1000], true )))
		local im2 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n100], true )))
		local im3 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n10], true )))
		local im4 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n1], true )))

		pImage:addChild(im1)
		pImage:addChild(im2)
		pImage:addChild(im3)
		pImage:addChild(im4)

		im1:setPosition(0,0)
		im2:setPosition(im1:getX() + im2:getWidth(), 0)
		im3:setPosition(im2:getX() + im3:getWidth(), 0)
		im4:setPosition(im3:getX() + im4:getWidth(), 0)

	elseif n100 > 0 then 
		local im1 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n100], true )))
		local im2 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n10], true )))
		local im3 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n1], true )))
		pImage:addChild(im1)
		pImage:addChild(im2)
		pImage:addChild(im3)

		im1:setPosition(0,0)
		im2:setPosition(im1:getX() + im2:getWidth(), 0)
		im3:setPosition(im2:getX() + im3:getWidth(), 0)
	
	elseif n10 > 0 then 
		local im1 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n10], true )))
		local im2 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n1], true )))

		pImage:addChild(im1)
		pImage:addChild(im2)

		im1:setPosition(0,0)
		im2:setPosition(im1:getX() + im2:getWidth(), 0)

	else
		local im1 = Bitmap.new(TextureRegion.new(Texture.new(ColorNumbers[n1], true )))
		pImage:addChild(im1)
		im1:setPosition(0,0)
	end 

	self:addChild(pImage)
	pImage:setPosition(X_HIGH -100, 10)
end 


local function getAnElement(t)
	for k,v in pairs(t) do
		return k,v
	end
end


function BaloonGame:setBlast(item,ball)
	printxx("baloongamer -- ", "setBlast")
	printz(" - - SET BLAST - - ")
	local queue = {}
	local visited = {}
	
	queue[item.i * 1000 + item.j] = true
	
	while true do
		local k = getAnElement(queue)
		if k == nil then break end
		
		queue[k] = nil
		
		local i = math.floor(k / 1000)
		local j = k % 1000
		
		--print(i, j)
		
		
		if not visited[k] then
			visited[k] = true
		
			local list = {}
			self:getNeighbourCellsType(i,j,list,ball.type)
	 
			
			for c=1,#list do
				local index = list[c].i * 1000 + list[c].j
			--	print("while set blast list", k, list[c].i,list[c].j, Balon[list[c].i][list[c].j].type == ball.type  )
				queue[index] = true
			end 
		end	
	end

	local list = {}

	while true do
		local k = getAnElement(visited)
		if k == nil then break end
		
		visited[k] = nil
		local i = math.floor(k / 1000)
		local j = k % 1000	
	--	print("while", k, i,j, Balon[i][j].type == 1 )
		
		list[#list + 1] = { i = i, j = j}
	end 
	
	self.blastingBallsList = {}
	self.delayTween = {}
	if #list >=3 then  
		self.movieClipList = {}
		self.frames = {}
		for k = 1, #list do
			print("Yok olacaklar tam liste",list[k].i, list[k].j )
			Balon[list[k].i][list[k].j].on = false
		--	Balon[list[k].i][list[k].j].image:setVisible(false)
			Balon[list[k].i][list[k].j].exits = false 
			self.blastingBallsList[k] = {i = list[k].i, j = list[k].j}
	
			self.frames = {}
			for i=1,5 do 
				self.frames[#self.frames + 1] =  Bitmap.new(TextureRegion.new(Texture.new(blastImageList[i], true )))
				--self.frames[i] = self.frames[#self.frames ]
				self.frames[#self.frames]:setAnchorPoint(0.5, 0.5)
			end 	
			local n = #self.frames
		
			
			local xx = Balon[list[k].i][list[k].j].xx
			local yy = Balon[list[k].i][list[k].j].yy
			

			self.movieClipList[k]  = MovieClip.new{
				{1, 5, self.frames[n-4]},	
				{5, 10, self.frames[n-3]},	
				{11, 15, self.frames[n-2]},	
				{16, 20, self.frames[n-1]},	
				{21, 25, self.frames[n]},	
			}
			self.movieClipList[k]:setPosition(xx,yy)	
			
			self.qSprite:addChild(self.movieClipList[k])
			
			self.movieClipList[k]:addEventListener("complete",
					function () 
						self.qSprite:removeChild(self.movieClipList[k])
					end, self)
			self.movieClipList[k ].dispatchEvents = true						
			
					
			self.oldSprites[#self.oldSprites  + 1] = Balon[list[k].i][list[k].j].image
			--printa("yok olan aynı renk balonlar", list[k].i, list[k].j )
			
			--Level[self.levelNo].puan = Level[self.levelNo].puan + 10  
		end 
		self.blastingBallAnimationOn = true 
		local p = #list 
		Level[self.levelNo].puan = Level[self.levelNo].puan +  p * p
		self.tempPuan = p * p	
		
		local num = 0
		if self.tempPuan > 45 and self.tempPuan < 60 then 
			num = 1

		elseif self.tempPuan >=60 and self.tempPuan < 80 then
			num = 2
		elseif self.tempPuan >= 80 and self.tempPuan < 100 then
			num = 3
		elseif self.tempPuan >=100 then 
			num = 4
		end 
		if num > 0 then 
			self.soundFile4 = cheerSounds[num]
			self.ss4 = Sound.new(self.soundFile4)
			self.soundChannel4  = self.ss4:play()	
			self.soundChannel4:setVolume(gameVolume)
		end 
			
		
		if p >= 1 then 
			printa("Yok olan balonlar", p, p * p )
			self:puanAnimation1(self.tempPuan, Balon[list[1].i][list[1].j].xx, Balon[list[1].i][list[1].j].yy )
		end 			
	end 

	
end 

function BaloonGame:smallAnimation(k, xx,yy)
	printxx("baloongamer -- ", "smallAnimation")
	printz(" small animation " , " 1" )

	for i=1,5 do 
		local index = #self.frames2 + 1
		self.frames2[index ] =  Bitmap.new(TextureRegion.new(Texture.new(smallImageList[i], true )))
		self.frames2[index]:setAnchorPoint(0.5, 0.5)
	end 	

	printz(" small animation " , " 2" )
	index = #self.frames2 - 4
	
	self.movieClipList2[k]  = MovieClip.new{
						{1, 3, self.frames2[index]},	
						{3, 6, self.frames2[index + 1]},	
						{6, 9, self.frames2[index +2]},	
						{9, 12, self.frames2[index +3]},	
						{12, 15, self.frames2[index +4]},	
					}
					
					
	self.movieClipList2[k]:addEventListener("complete",
			function () 
				--self.movieClipList2[k]:setAlpha(0)
			end, self)
	self.movieClipList2[k ].dispatchEvents = true	

	
	self.movieClipList2[k]:setPosition(xx,yy)
	self.qSprite:addChild(self.movieClipList2[k])

end 

function BaloonGame:calculateHit()
	--printz(" - - CALCULATE HIT - - ")
	printxx("baloongamer -- ", "calculateHit")
	printa("------------------CALCULATE HIT ------------------")

	local j,i = pixel_to_grid(self.currentBaloon.x, self.currentBaloon.y)
--	print("pixel to GRID", i,j)
	local list = {}
	local isHit = false 
	local hitPlace = {}
	self:getNeighbourCells(i,j,list,true)
	self.nTweenList = {}
	self.movieClipList2 = {}
	self.frames2 = {}

	
	for c = 1, #list do
		local x1 = Balon[list[c].i][list[c].j].xx
		local y1 = Balon[list[c].i][list[c].j].yy

		local distance = (self.currentBaloon.x - x1) * (self.currentBaloon.x - x1)  + (self.currentBaloon.y - y1) * (self.currentBaloon.y - y1)  

		if distance < 3600 then 	--hit
			isHit = true 
			hitPlace = {i = list[c].i, j = list[c].j}
			self.movingBall = false	
			ballPlace = {i = i, j = j, type = self.currentBaloon.type }
			printx(" ----------------------------------------------- calculating hit ---------------------------------")
			printx("hit place", list[c].i, list[c].j, "ball place", i, j)
	
			--printa("The ball is hit in ", ballPlace.i, ballPlace.j)
			if ballPlace.j <= 0 then 
				ballPlace.j = 1 
				if Balon[ballPlace.i][ballPlace.j].on and Balon[ballPlace.i][ballPlace.j].exits then 
					printx("--: 0 a denk geldi ve dolu")
					ballPlace.j = 1
					ballPlace.i = ballPlace.i  + 1
					if Balon[ballPlace.i][ballPlace.j].on and Balon[ballPlace.i][ballPlace.j].exits then 
						printx("--: 0 2. kez a denk geldi ve dolu", ballPlace.i, ballPlace.j)
						ballPlace.j = 2
						ballPlace.i = ballPlace.i  
			
					end
				else
					printx("--: 0 a denk geldi boş")	
				end 
			end 
			
			if ballPlace.j >= 10 then 
				ballPlace.j = 10 
				if Balon[ballPlace.i][ballPlace.j].on and Balon[ballPlace.i][ballPlace.j].exits then 
					printx("--: 10 a denk geldi ve dolu")
					ballPlace.j = 10
					ballPlace.i = ballPlace.i  + 1
					if Balon[ballPlace.i][ballPlace.j].on and Balon[ballPlace.i][ballPlace.j].exits then 
						printx("--: 10 2. kez a denk geldi ve dolu", ballPlace.i, ballPlace.j)
						ballPlace.j = 9
						ballPlace.i = ballPlace.i  
			
					end
				else
					printx("--: 10 a denk geldi boş")	
				end 				
			end 
			
			printa("after" , ballPlace.i, ballPlace.j)

			for m = 1,3 do
				self.currentBaloons[m]:setVisible(false)
			end 
			self.newBaloons[#self.newBaloons-2]:setVisible(true)
			Balon[ballPlace.i][ballPlace.j].on = true
	--		Balon[ballPlace.i][ballPlace.j].image = self.newBaloons[#self.newBaloons]	
			Balon[ballPlace.i][ballPlace.j].image = self.newBaloons[#self.newBaloons-2]
			Balon[ballPlace.i][ballPlace.j].image:setPosition(Balon[ballPlace.i][ballPlace.j].xx, Balon[ballPlace.i][ballPlace.j].yy)
			--Balon[ballPlace.i][ballPlace.j].image:setColorTransform(1,1,0,1)
			Balon[ballPlace.i][ballPlace.j].type = self.currentBaloon.type 
		
			Balon[ballPlace.i][ballPlace.j].exits = true 
			self:setBlast(ballPlace, ballPlace)
	
			break
		end	
	end 
	
	if not isHit then 
		local y = Balon.up
		--printa("balon.up", y)
		local temp = #Balon[y]
		for c = 1, #Balon[y] do
			local x1 = Balon[y][c].xx
			local y1 = Balon[y][1].yy
			--printa("x1", x1, "y1", y1)
			local distance = (self.currentBaloon.x - x1) * (self.currentBaloon.x - x1)  + (self.currentBaloon.y - y1) * (self.currentBaloon.y - y1)  

			if distance < 3600 then 	--hit
				isHit = true 
				hitPlace = {i = y, j = c}
				self.movingBall = false	
				ballPlace = {i = y, j = c, type = self.currentBaloon.type }
				print("NO HIT ball is", y,j,self.currentBaloon.type )
			
				if ballPlace.j <= 0 then ballPlace.j = 1 end 
				if ballPlace.j >= 10 then ballPlace.j = 10 end 

				for t = 1,3 do
					self.currentBaloons[t]:setVisible(false)
				end 
				
				self.newBaloons[#self.newBaloons-2]:setVisible(true)
				Balon[ballPlace.i][ballPlace.j].on = true
			--	Balon[ballPlace.i][ballPlace.j].image = self.newBaloons[#self.newBaloons]	
				Balon[ballPlace.i][ballPlace.j].image = self.newBaloons[#self.newBaloons-2]
				Balon[ballPlace.i][ballPlace.j].image:setPosition(Balon[ballPlace.i][ballPlace.j].xx, Balon[ballPlace.i][ballPlace.j].yy)
				--Balon[ballPlace.i][ballPlace.j].image:setColorTransform(1,1,0,1)
				Balon[ballPlace.i][ballPlace.j].type = self.currentBaloon.type 
				
	
				Balon[ballPlace.i][ballPlace.j].exits = true 		
				if ballPlace.i > Balon.down then
					Balon.down = ballPlace.i 
				end 
		
				self:setBlast(ballPlace, ballPlace)
			

				break
			end	
		end 
	
	end 


	
	if isHit then 
		printz(" - - IS HIT - - ")
		self:blastLonelyBaloons()


		
		printz("in hit")
		self:checkScollDown()
		self.mouseOn = true 	
		
		self:newBall()
		--printa("The SCOR IS.......... ", self.levelNo, " . . SCOR : .. ", Level[self.levelNo].puan)
	end 
		self:checkNext()	
		--self:printList()
end 


function BaloonGame:checkNext()
	printxx("baloongamer -- ", "checkNext")
	printz(" - - CHECKT NEXT - - ")
	totalBall = 0 
	
	local biggestFullY = 0
	
	for i = 18, Balon.up,-1 do
		local sira = false 
		for j = 1, #Balon[i] do
			if Balon[i][j].exits ==  true  then 
				totalBall = 	totalBall  + 1
				sira = true 
			end 		
		end 
		if sira == true then
			if i > biggestFullY then biggestFullY = i end 
		end 
	end 	

	printxx("  ------------------------------  check next ----------------------")
	printxx("TOPLAM top sayisi", totalBall)

	
	if totalBall < 2 then self:nextLevel() end 		
	
	

	if biggestFullY > 16 then 
		printxx("BaloonGame", "--prefail")
		self:fail()
		
	end  	
end 	







function BaloonGame:cleanOldSprite()
	printxx("baloongamer -- ", "cleanOldSprite")
	printz("cleaning")
	for i = 1, #self.oldSprites do
		if self.qSprite:contains(self.oldSprites[i]) then 
			printz("clean old Sprite", i)
			self.qSprite:removeChild(self.oldSprites[i])
		
		end 
	
	end 

end 




