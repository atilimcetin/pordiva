---------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------                    END             -------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------


function BaloonGame:endAnimation()


	self.soundFile = "sound/sampiyon.wav"
	self.ss = Sound.new(self.soundFile)
	self.soundChannel1  = self.ss:play()	
		self.soundChannel1:setVolume(gameVolume)


	self:addChild(self.subScreen)
	self:addChild(self.score)
	self.subScreen:setPosition(X_HIGH/2 , -500)
--	self.score = Bitmap.new(TextureRegion.new(Texture.new("gfx/skor.png", true )))
	--self.score:setAnchorPoint(0.5,0.5)
	self.score:setPosition(X_HIGH/2, -500)
	
	local yy = 150
	
	self.stars = Bitmap.new(TextureRegion.new(Texture.new("gfx/emptystar.png", true )))
	self.stars:setPosition(self.subScreen:getWidth() / 2, self.subScreen:getHeight()/2+70 - yy)
	self.stars:setAnchorPoint(0.5, 0.5)
	self.stars:setAlpha(0)
	self:addChild(self.stars)

	
	self.oyna = Bitmap.new(TextureRegion.new(Texture.new("gfx/oyna.png", true )))
	self.oyna:setPosition(self.subScreen:getWidth() / 2, self.subScreen:getHeight() - 25)
	self.oyna:setAnchorPoint(0.5, 0.5)
	self.oyna:setAlpha(0)
	self:addChild(self.oyna)
	


	self.st = {}
	self.st[1] = Bitmap.new(TextureRegion.new(Texture.new("gfx/star.png", true )))
	self.st[1]:setPosition(-100,18-yy)
	self.st[1]:setAnchorPoint(0.5, 0.5)
	self.st[1]:setAlpha(0)
	self.stars:addChild(self.st[1])
	self.st[1].x = -100
	self.st[1].y = 18
	
	self.st[2] = Bitmap.new(TextureRegion.new(Texture.new("gfx/star.png", true )))
	self.st[2]:setPosition(0,-14-yy)
	self.st[2]:setAnchorPoint(0.5, 0.5)
	self.st[2]:setAlpha(0)
	self.stars:addChild(self.st[2])
	self.st[2].x = 0
	self.st[2].y = -14


	self.st[3] = Bitmap.new(TextureRegion.new(Texture.new("gfx/star.png", true )))
	self.st[3]:setPosition(100,18-yy)
	self.st[3]:setAnchorPoint(0.5, 0.5)
	self.st[3]:setAlpha(0)
	self.stars:addChild(self.st[3])
	self.st[3].x = 100
	self.st[3].y = 18
	
	for i =1,3 do
		self.st[i]:setScale(3)
	end 


	local t1 = 0.5
	local t2 = 0.4
	local t3 = 0.4


	self.tw = GTween.new(self.subScreen, t1 , {alpha = 1, x = X_HIGH/2, y = Y_HIGH / 2 - 100 }, { ease = easing.outBack, delay = 0, repeatCount = 1, reflect = false } )
	self.tw2 = GTween.new(self.score, t2 , {alpha = 1, x = X_HIGH/2, y = Y_HIGH / 2+50 -yy }, { ease = easing.outBack, delay =t1, repeatCount = 1, reflect = false } )
	self.tw3 = GTween.new(self.stars, t2 , {alpha = 1, scaleX = 1.2, scaleY = 1.2}, { ease = easing.outBack, delay =t1+ t2, repeatCount = 1, reflect = false } )




	self.tst1 = GTween.new(self.st[1], t3 , {alpha = 1, x = self.st[1].x, y = self.st[1].y , scaleX = 1, scaleY = 1}, { ease = easing.exponential, delay = t1+ t2* 2, repeatCount = 1, reflect = false } )
	self.tst2 = GTween.new(self.st[2], t3 , {alpha = 1, x = self.st[2].x, y = self.st[2].y , scaleX = 1, scaleY = 1}, { ease = easing.exponential, delay = t1+t2*2+t3, repeatCount = 1, reflect = false } )
	self.tst3 = GTween.new(self.st[3], t3 , {alpha = 1, x = self.st[3].x, y = self.st[3].y , scaleX = 1, scaleY = 1}, { ease = easing.exponential, delay = t1+t2*2+t3*2, repeatCount = 1, reflect = false } )
	
	self.tst4 = GTween.new(self.oyna, t3 , {alpha = 1}, { ease = easing.exponential, delay = t1+t2*2+t3*3 , repeatCount = 1, reflect = false } )	
	
	
	local no = Level[self.levelNo].puan
	local pString = tonumber(no)
	
	local tt = {}	
--[[	local tttw = {}
	local count = 10
	for i = 1, no/10 do
		local pString = tonumber(count)
		count = count + 10 
		tt[i] = TextField.new(ff, pString)
		tt[i]:setTextColor(0x0)
		self.score:addChild(tt[i])
		--tt:setAnchorPoint(0.5,0.5)
		tt[i]:setPosition(-30,20)
		tt[i]:setAlpha(0)
		--if i > 1 then self.score:removeChild(tt[i-1]) end 
		tttw[i] = GTween.new(tt[i], 0.5 , {alpha = 1}, { ease = easing.exponential, delay = 2.0 + i*0.1, repeatCount = 2, reflect = true } )
	
	end ]]--
	
	local pString = tonumber(no)
		--count = count + 10 
		tt[1] = TextField.new(ff, pString)
		tt[1]:setTextColor(0x0)
		self.score:addChild(tt[1])
		--tt:setAnchorPoint(0.5,0.5)
		tt[1]:setPosition(-30,20)
		tt[1]:setAlpha(0)
		--if i > 1 then self.score:removeChild(tt[i-1]) end 
		GTween.new(tt[1], 1 , {alpha = 1}, { ease = easing.exponential, delay = 2.0 , repeatCount = 1, reflect = false } )	




end 


function BaloonGame:fail()
	printxx("baloongame--", "fail", self.successState)
	if self.successState ~= 1 then  
		self.successState = -1 
		printa("FAIL") 
		self.gameEnd = true 
	--	self.qSprite:setColorTransform(0.8, 0.8, 0.2, 1)
		self.qSprite:setAlpha(0.6)
		self.mouseOn = false
		self.movingBall = false 
		self.failing = false 
		self:endFail()	
	end 
end 

function BaloonGame:nextLevel(item,ball)
	printz(" - - NEXT LEVEL - - ")
	printxx("baloongame--", "nextLevel", self.successState)	
	if  self.successState ~= -1 then 
	
		self.gameEnd = true 
	--	self.qSprite:setColorTransform(0.8, 0.8, 0.2, 1)
		self.qSprite:setAlpha(0.6)
		self.mouseOn = false
		self.movingBall = false 
		
		self.successState  = 1 
		
		self:endAnimation()
	end 	

end 




function BaloonGame:endFail()
	printa("end fail")
	self.failing  = true 
	if self:contains(self.subScreen) then 
		self:removeChild(self.subScreen)
	end 

	self.soundFile = "sound/boo2.wav"
	self.ss = Sound.new(self.soundFile)
	self.soundChannel1  = self.ss:play()	
	self.soundChannel1:setVolume(gameVolume)

	self.failfail = Bitmap.new(TextureRegion.new(Texture.new("gfx/kazanama.png", true )))
	self.failfail:setPosition(0,0 )
	self.failfail:setAnchorPoint(0.5, 0.5)
	self.failfail:setAlpha(0)
	self.failfail:setScale(5)
--	self:addChild(self.failfail)
	self.retry = Bitmap.new(TextureRegion.new(Texture.new("gfx/Loose.png", true )))
	self.retry:setAnchorPoint(0.5, 0.5)
	self.retry:setPosition(X_HIGH/2 , -500 )
	self.retry:setScale(1, 1)
	
	self.oyna = Bitmap.new(TextureRegion.new(Texture.new("gfx/oyna.png", true )))
	self.oyna:setPosition(0,200)
	self.oyna:setAnchorPoint(0.5, 0.5)
	self.oyna:setAlpha(0)
	self.retry:addChild(self.oyna)	
	
	self.retry:setAlpha(1)
	self.retry:addChild(self.failfail)
	self:addChild(self.retry)
	
	self.tw = GTween.new(self.retry, 1 , {alpha = 1, x = X_HIGH/2, y = Y_HIGH / 2 - 100 }, { ease = easing.outBack, delay = 0, repeatCount = 1, reflect = false } )
	self.tw2 = GTween.new(self.failfail, 1 , {alpha = 1 , scaleX = 0.9, scaleY = 0.9 }, { ease = easing.outBack, delay = 1, repeatCount = 1, reflect = false } )
	self.tst4 = GTween.new(self.oyna, 1 , {alpha = 1}, { ease = easing.exponential, delay = 2 , repeatCount = 1, reflect = false } )	
	

--[[	self.tw = GTween.new(self.failfail, 1 , {alpha = 0.8 , scaleX = 1, scaleY = 1 }, { ease = easing.exponential, delay = 0, repeatCount = 1, reflect = false } )

self.tw2 = GTween.new(self.failfail, 1 , {alpha = 0, scaleX = 0.01, scaleY = 0.01 }, { ease = easing.exponential, delay = 2, repeatCount = 1, reflect = false } )
	self.tw2:addEventListener("complete", function () 
		self:addChild(self.subScreen2)
		self.subScreen2:setPosition(X_HIGH/2 , -500)
		self.tw3 = GTween.new(self.subScreen2, 1 , {alpha = 1, x = X_HIGH/2, y = Y_HIGH / 2 - 100 }, { ease = easing.exponential, delay = 0, repeatCount = 1, reflect = false } )
		self.tw4 = GTween.new(self.retry, 1 , {scaleX = 0.8, scaleY = 0.8 }, { ease = easing.exponential, delay = 0.9, repeatCount = 1, reflect = false } )
	
		self.subScreen2:addChild(self.retry)
		
	end, self )

	self.tw2.dispatchEvents = true ]]--
	

end 
