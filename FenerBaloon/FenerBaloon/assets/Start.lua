
Page0 = gideros.class(Sprite)

local startB = "gfx/kapak3.png"

X_HIGH = 640
Y_HIGH = 1136

currentLevel = 1

levelmirrorPos = {}

levelmirrorPos[1] = {x = -70, y = 1116}
levelmirrorPos[2] = {x = 135+5, y = 1050-5}
levelmirrorPos[3] = {x = 325+3, y = 970+5}
levelmirrorPos[4] = {x = 40+7, y = 927}
levelmirrorPos[5] = {x = -205, y = 970-10}
levelmirrorPos[6] = {x = -355+10, y = 970+100+10-5}
levelmirrorPos[7] = {x = -410, y = 890}
levelmirrorPos[8] = {x = -170+4, y = 777}
levelmirrorPos[9] = {x = 200+20-5, y = 790}

levelmirrorPos[10] = {x = 180+20, y = 620+20}
levelmirrorPos[11] = {x = -20, y = 690+10}
levelmirrorPos[12] = {x = -270-4+4, y = 545-2}
levelmirrorPos[13] = {x = -350-5, y = 410+10+2+4}
levelmirrorPos[14] = {x = -110-5, y = 440+2}
levelmirrorPos[15] = {x = 140, y = 430}
levelmirrorPos[16] = {x = 330, y = 385}
levelmirrorPos[17] = {x = 300+15, y = 200}
levelmirrorPos[18] = {x = 150, y = 240+50}
levelmirrorPos[19] = {x = -30-2, y = 180}

levelmirrorPos[20] = {x = -220, y = 310}
levelmirrorPos[21] = {x = -280-2, y = 160}
levelmirrorPos[22] = {x = -380-2, y = 50}
levelmirrorPos[23] = {x = -38-1, y = 40}
levelmirrorPos[24] = {x = 220, y = 45}
levelmirrorPos[25] = {x = 300, y = -90}
levelmirrorPos[26] = {x = 100, y = -50}
levelmirrorPos[27] = {x = -20 , y = -170}
levelmirrorPos[28] = {x = -300, y = -130}
levelmirrorPos[29] = {x = -400, y = -250}


levelmirrorPos[30] = {x = -185, y = -250}
levelmirrorPos[31] = {x = -140, y = -380}
levelmirrorPos[32] = {x = 170, y = -350+20}
levelmirrorPos[33] = {x = 290, y = -240}
levelmirrorPos[34] = {x = 270, y = -490}
levelmirrorPos[35] = {x = 5, y = -470+10}
levelmirrorPos[36] = {x = -270, y = -520}
levelmirrorPos[37] = {x = -380 , y = -650}
levelmirrorPos[38] = {x = -80, y = -600}
levelmirrorPos[39] = {x = 150, y = -650}


levelmirrorPos[40] = {x = 275, y = -740}
levelmirrorPos[41] = {x = -65, y = -850+50}
levelmirrorPos[42] = {x = -320, y = -800-20}
levelmirrorPos[43] = {x = -275+3, y = -950+5}
levelmirrorPos[44] = {x = -300-70, y = -1000-50}
levelmirrorPos[45] = {x = -100+30, y = -950-50}
levelmirrorPos[46] = {x = 180, y = -950+20}
levelmirrorPos[47] = {x = 300 , y = -1000-70}
levelmirrorPos[48] = {x = 35, y = -1100-60}
levelmirrorPos[49] = {x = -200-10, y = -1160+20}

levelmirrorPos[50] = {x = -150, y = -1200-100}

levelSelectorData = {}
for i=1,10 do
	levelSelectorData[i] = { x = 0, y = -650+200 +50}
end 	

for i=11,11 do
	levelSelectorData[i] = { x = 0, y = -500 }
end 	

for i=12,12 do
	levelSelectorData[i] = { x = 0, y = -400 }
end 	

for i=13,13 do
	levelSelectorData[i] = { x = 0, y = -200 }
end 	

for i =14,15 do
	levelSelectorData[i] = { x = 0, y = -200 }
end 

levelSelectorData[16] = { x = 0, y = -150 }
	
for i=17,19 do
	levelSelectorData[i] = { x = 0, y = 100 }
end 	

for i = 20, 21 do

	levelSelectorData[i] = { x = 0, y = 150 }
end 
	
for i = 22, 24 do

	levelSelectorData[i] = { x = 0, y = 400 }
end 
		
	
for i = 25, 29 do

	levelSelectorData[i] = { x = 0, y = 600 }
end 	
	
for i=30,33 do
	levelSelectorData[i] = { x = 0, y = 800 }
end 	

for i=34,42 do
	levelSelectorData[i] = { x = 0, y = 1200 }
end 

for i=43,50 do
	levelSelectorData[i] = { x = 0, y = 1400 }
end 

levelSelectorData[50] = { x = 0, y = 1550 }

local sag = {2, 3,9,10,15,16,18,17,24,25,33,32,34,39,40,46,47}
local orta = {1,4,5,8,11,14,20,19,23,26,27,30,31,35,38,41,45,48,49,50}
local sol = {6,5,7,12,13,20,21,22,28,29,36,37,42,43,44}

for i=1, #sag do
	levelSelectorData[sag[i]].x = X_HIGH /2 - 200
end 

for i=1, #orta do
	levelSelectorData[orta[i]].x = X_HIGH /2
end 

for i=1, #sol do
	levelSelectorData[sol[i]].x = X_HIGH /2+ 200
end 

function Page0:init()

	bk = Bitmap.new(TextureRegion.new(Texture.new(startB, true )))
	bk:setAnchorPoint(0.5, 0.5)
	self:addChild(bk)
	bk:setPosition(X_HIGH /2 , Y_HIGH / 2)
	
	oyna = Bitmap.new(TextureRegion.new(Texture.new("gfx/oyna.png", true )))
	oyna:setAnchorPoint(0.5, 0.5)
	self:addChild(oyna)
	oyna:setPosition(X_HIGH /2+12 , Y_HIGH / 2+450)
	--oyna:setAlpha(0.5)



	self.soundFile = "sound/fbint.mp3"
	self.ss = Sound.new(self.soundFile)
	self.soundChannel1  = self.ss:play()	
	self.soundChannel1:setVolume(gameVolume)
	self:addEventListener(Event.MOUSE_UP, self.onMouseUp, self)
end 	

function Page0:onMouseUp(event)
	ev= Event.new("hebe")
	ev.page = 2
	ev.level = currentLevel
	self.soundChannel1:stop()
	tw = GTween.new(oyna, 0.2 , {alpha = 1, scaleX = 1.2, scaleY = 1.2}, { ease = easing.exponential, delay = 0, repeatCount = 2,	reflect = true } )
	tw:addEventListener("complete", function()  
		self.manager:gotoPage(ev)
	end, self )
	tw.dispatchEvents = true 
end 


Page1 = gideros.class(Sprite)

local startC = "gfx/levelselector3.png"
local startD = "gfx/shadow.png"

function Page1:init(levelno, oldlevelno)
	printxx("***************************************************************************************************************", levelno, oldlevelno)

	self.bk = Bitmap.new(TextureRegion.new(Texture.new(startC, true )))
	self.bk:setAnchorPoint(0.5, 0.5) 
--	bk:setScale(0.5, 0.5)
	x = X_HIGH / 2
	y = -150
	
	if levelno ~= nil then 
		currentLevel = levelno
	end 	
		
	
	self.levelMirror = {}
	
	for i =1, 50 do
		self.levelMirror[i] = Bitmap.new(TextureRegion.new(Texture.new("gfx/levelmirror.png", true )))
		self.levelMirror[i]:setPosition(levelmirrorPos[i].x , levelmirrorPos[i].y)
		self.bk:addChild(self.levelMirror[i])
		self.levelMirror[i]:setAlpha(0)
	end 
	
	self.shadows = {}
	for i = 1,50 do
		self.shadows[i] = Bitmap.new(TextureRegion.new(Texture.new(startD, true )))
		self.shadows[i]:setAnchorPoint(0.5,0.5)
		self.shadows[i]:setPosition(levelmirrorPos[i].x+60-5 , levelmirrorPos[i].y+80+4)
		self.bk:addChild(self.shadows[i])
		self.shadows[i]:setAlpha(0)
	end 
	printa(currentLevel)
	if currentLevel > 100 then 
		currentLevel = currentLevel - 100
	end	
	x1 = levelSelectorData[currentLevel].x
	y1 = levelSelectorData[currentLevel].y -20
	printa("level", currentLevel, x, y)
	
	x2 = levelmirrorPos[currentLevel].x
	y2 = levelmirrorPos[currentLevel].y-25
	
	

	printa(currentLevel,x1,y1,x2,y2)
	self.bk:setX(x)
	self.bk:setY(y)
	self:addChild(self.bk)
	
	
	for i = 1, currentLevel - 1 do 
	
		self.shadows[i]:setAlpha(0.5)
	end 

	if oldlevelno ~= nil then 
		printa("not nil")
		printxx(" BURADAYIZ 1A ")
		printxx(currentLevel, oldlevelno)
		
		x1 = (levelSelectorData[currentLevel].x + levelSelectorData[oldlevelno].x) / 2
		y1 = (levelSelectorData[currentLevel].y + levelSelectorData[oldlevelno].y) / 2 + 10
		
		
		printa(currentLevel,x1,y1,x2,y2)
		tw = GTween.new(self.bk, 1 , {alpha = 1, x = x1-50, y = y1+20, scaleX = 1.2, scaleY = 1.2 }, { ease = easing.linear, delay = 0, repeatCount = 1, reflect = false } )
		local d = 1
		printxx(" BURADAYIZ 1A a ")
		tw3 = GTween.new(self.shadows[oldlevelno], 0.4 , {alpha = 0.5, scaleX = 1.2, scaleY = 1.2 }, { ease = easing.linear, delay = d, repeatCount = 2, reflect = true } )
		tempShadow = Bitmap.new(TextureRegion.new(Texture.new(startD, true )))
		tempShadow:setAnchorPoint(0.5,0.5)
		tempShadow:setPosition(levelmirrorPos[oldlevelno].x+60-5 , levelmirrorPos[oldlevelno].y+80+4)
		self.bk:addChild(tempShadow)
		xx = levelmirrorPos[currentLevel].x+60-5 
		yy = levelmirrorPos[currentLevel].y+80+4
		tw4 = GTween.new(self.shadows[oldlevelno], 1 , {alpha = 0.5, scaleX = 1.2, scaleY = 1.2 , x = xx, y = yy  }, { ease = easing.linear, delay = 0, repeatCount = 1, reflect = false } )
		printxx(" BURADAYIZ 1A b")

		tw4:addEventListener("complete", function()  
				printxx(" BURADAYIZ 1A c")
				tw2 = GTween.new(self.shadows[currentLevel], 0.4 , {alpha = 0.5, scaleX = 1.2, scaleY = 1.2 }, { ease = easing.linear, delay = 0, repeatCount = 4, reflect = true } )
				self.shadows[oldlevelno]:setAlpha(0.5)
				tw2:addEventListener("complete", function()  
						printxx(" BURADAYIZ 1A d")
						ev= Event.new("hebe")
						ev.page = 3
						ev.level = currentLevel + 100
						self.manager:gotoPage(ev)
						printa(self.bk:getX(), self.bk:getY())
				end, self )
				tw2.dispatchEvents = true 			
				printxx(" BURADAYIZ 1A f")
		end, self )
		tw4.dispatchEvents = true 			
		printxx(" BURADAYIZ 1A e", tw4, self.shadows[oldlevelno], oldlevelno)
	else 
			printxx("Burdayız 2A")
			
			
			self.shadows[currentLevel]:setAlpha(0.5)
			self.shadows[currentLevel]:setX(self.shadows[currentLevel]:getX()-5)
			self.shadows[currentLevel]:setY(self.shadows[currentLevel]:getY()+5)
			tw2 = GTween.new(self.shadows[currentLevel], 0.4 , {alpha = 0.8, scaleX = 1.3, scaleY = 1.3 }, { ease = easing.linear, delay = 0, repeatCount = 1000, reflect = true } )
--			self.ttlist = {}
--			for mm= 1,50 do 
--				self.shadows[mm]:setAlpha(0.5)
--				self.shadows[mm]:setX(self.shadows[mm]:getX())
--				self.shadows[mm]:setY(self.shadows[mm]:getY())
				--self.ttlist[mm] = GTween.new(self.shadows[m], 0.4 , {alpha = 0.8, scaleX = 1.3, scaleY = 1.3 }, { ease = easing.linear, delay = 0, repeatCount = 1000, reflect = true } )
--			end 
	
			
			self.start = {x = 0, y = 0}
			
			tw = GTween.new(self.bk, 1 , {alpha = 1, x = x1, y = y1, scaleX = 1.2, scaleY = 1.2 }, { ease = easing.linear, delay = 0, repeatCount = 1, reflect = false } )
			tw:addEventListener("complete", function()  
				self:addEventListener(Event.MOUSE_UP, self.onMouseUp, self)
				self:addEventListener(Event.MOUSE_DOWN, self.onMouseDown, self)
				self:addEventListener(Event.MOUSE_MOVE, self.onMouseMove, self)
			end, self )
			tw.dispatchEvents = true 	
			
	end 	
end 	

function Page1:onMouseMove(event)
	self.deltax = event.x - self.start.x
	self.deltay = event.y - self.start.y 
	
	self.newX = self.bk:getX() + self.deltax / 4
	self.newY = self.bk:getY() + self.deltay / 4
	
	xmin = X_HIGH /2 - 150 -30
	xmax = X_HIGH /2 + 150 +30
	ymin = -700+50+200+50
	ymax = 1300+500-50-200-50
	
	if self.newX > xmax then self.newX = xmax end 
	if self.newX < xmin then self.newX = xmin end 
	if self.newY > ymax then self.newY = ymax end 
	if self.newY < ymin then self.newY = ymin end 	
	
--	printxx("--",self.newX, self.newY)
	self.bk:setPosition(self.newX, self.newY)

end 

function Page1:onMouseDown(event)
	self.start.x = event.x
	self.start.y = event.y 

end



function Page1:onMouseUp(event)
	local selected = 0
	for i =1,50 do
		if self.levelMirror[i]:hitTestPoint(event.x, event.y) == true then 
			selected = i
			break
		end 
	end 

	if selected > 0 and selected <= currentLevel then 

		ev= Event.new("hebe")
		ev.page = 3
		ev.level =100 + currentLevel
		self.manager:gotoPage(ev)
		printa(self.bk:getX(), self.bk:getY())
		
	end 	
end 



--[[
currentLevel = 1

function Page1:onMouseUp(event)
	local selected = 0
	for i =1,50 do
		if self.levelMirror[i]:hitTestPoint(event.x, event.y) == true then 
			selected = i
			break
		end 
	end 

	if selected > 0 and selected <= currentLevel then 

		ev= Event.new("hebe")
		ev.page = 2
		ev.level =100 + currentLevel+1
		
		self.manager:gotoPage(ev)
		printa(self.bk:getX(), self.bk:getY())
		
	end 	
end ]]--