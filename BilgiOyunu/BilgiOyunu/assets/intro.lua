IntroScreen = gideros.class(Sprite)

fanPeople = {}

function IntroScreen:init()
	c1 = os.clock()


	self.bp1  =  Bitmap.new(TextureRegion.new(Texture.new("gfx/fbserit.png", true )))
	self:addChild(self.bp1)
	self.bp1:setScaleX(200)
	self.bp1:setX(-400)
	
	self.bp2 = Bitmap.new(TextureRegion.new(Texture.new("gfx/minigiris.png", true )))
	self.bp2:setAlpha(1)
	self.bp2:setPosition(X_HIGH / 2, Y_HIGH / 2)
	self.bp2:setScale(3,3)
	self.bp2:setAnchorPoint(0.5, 0.5)
	self:addChild(self.bp2)
--	self.bp2:setRotation(60)
	
	self.soundFile = "sound/fb_.wav"
	self.ss = Sound.new(self.soundFile)
	self.soundChannel1  = self.ss:play()	
	self.people = {}
	

	
	self.tween = GTween.new(self.bp2, 3, {alpha = 1, scaleX = 1.2, scaleY  = 1.2, rotation = 0 }, { ease = easing.outBack, delay = 4, repeatCount = 1, reflect = false } )				
--[[	self.tween2 = GTween.new(self.bp2, 1, {alpha = 1, scaleX = 1, scaleY  = 1, rotation = 30 }, { ease = easing.linear, delay = 2, repeatCount = 1, reflect = false } )				
	self.tween3 = GTween.new(self.bp2, 1, {alpha = 1, scaleX = 1.2, scaleY  = 1.2, rotation = 0 }, { ease = easing.linear, delay = 3, repeatCount = 1, reflect = false } )				
	self.tween4 = GTween.new(self.bp2, 0.5, {alpha = 1, scaleX = 1, scaleY  = 1, rotation = 0 }, { ease = easing.linear, delay = 4, repeatCount = 1, reflect = false } )				
	
]]--

	for i=1,6 do
		for j = 1,7 do
			if peopleCoordList[i][j] > 0 then 
				local person = peopleCoordList[i][j]
				self.people[#self.people + 1] = {}
				local index = #self.people
				fanPeople[index] = {}
				for k = 1, #peopleList[person] do
					fanPeople[index][k] = Bitmap.new(TextureRegion.new(Texture.new(peopleList[person][k], true )))
					print(" - - ", index, k, peopleList[person][k])
				end 
			end 	
		end 
	end	

	self:addEventListener(Event.MOUSE_DOWN, self.onMouseDown, self)

end 


function IntroScreen:onMouseDown(event)

	c2 = os.clock()
	d2 = c2 - c1 
	print("-------------------------------------------------------", d2)

	self.tween2 = GTween.new(self.bp2, 1, {alpha = 0, scaleX = 1.2, scaleY  = 1.2, rotation = 0 }, { ease = easing.outBack, delay = 4, repeatCount = 1, reflect = false } )				
--
	ev= Event.new("hebe")
	ev.page = 2
	
	
	self.manager:gotoPage(ev)
end 

