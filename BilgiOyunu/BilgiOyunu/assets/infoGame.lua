QuizGame = gideros.class(Sprite)

back = "gfx/stad_2048.png"

local X_HIGH  = 2048
local Y_HIGH  = 1536

allChars = [[ !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_``'’abcdefghijklmnopqrstuvwxyz{|}~İĞğüÜŞşiÇçÖöIı]]


fontF = TTFont.new( "font/Boogaloo-Regular.ttf", 76, allChars)
fontF2 = TTFont.new( "font/Boogaloo-Regular.ttf", 70, allChars)

fontSF = Font.new( "font/sf140.txt", "font/sf140.png")

trueDotPosition = {}
trueDotPosition[1] = { x = 930, y = 353} 
trueDotPosition[2] = { x = 973, y = 353} 
trueDotPosition[3] = { x = 973+43, y = 353} 
trueDotPosition[4] = { x = 973+43*2, y = 353} 
trueDotPosition[5] = { x = 973 + 43*3, y = 353} 


local soundYes = true 

function QuizGame:init()
	c3 = os.timer()
	d3 = c3 - c2
	print("---------------------------------------------------------  1  ", d3)
	self.trueCount = 0 
	
	self.points = 0
	self.qSprite = Sprite.new()
	self:loadBK()
	
	self.soundFileD = "sound/cheer4.wav"
	self.ssD = Sound.new(self.soundFileD)
	
	self.soundFileY = "sound/boo2.wav"
	self.ssY = Sound.new(self.soundFileY)
	
	local pNo = #self.people
	self.timer = Timer.new(50, pNo)
	
	self.timeCountdown = Timer.new(1000, 60)
	self.timeCountdown:addEventListener(Event.TIMER, self.finalCountdown, self)	
	
	self.timer2 = Timer.new(3000, 1)
	self.counter = 1

	self.randList = {}
	for i = 1,pNo do
		self.randList[i] = i 
	end 
	
	for i=1,50 do
		local index1 = math.random(1,pNo)
		local index2 = math.random(1,pNo)
		local temp = self.randList[index1]
		self.randList[index1] = self.randList[index2]
		self.randList[index2] = temp
	end 

	self.timer:addEventListener(Event.TIMER, self.onTimer, self)
	self.timer2:addEventListener(Event.TIMER, self.onTimer2, self)	
	
	self.questionList = {}

	for i = 1,105 do
		if i ~= 37 then 
			if Sorular[i].visited == false then 
				self.questionList[#self.questionList + 1] = i
			end 
		end 
	end 

	
	for  i = 1,200 do
		p1=math.random(1,#self.questionList)
		p2 =math.random(1,#self.questionList)
		temp = self.questionList[p1]
		self.questionList[p1]= self.questionList[p2]
		self.questionList[p2] = temp
	end 
	
	
	self.questionCounter = 1
--	self:askNewQuestion()

	self:addEventListener(Event.MOUSE_DOWN, self.onMouseDown, self)
	self:addEventListener(Event.ENTER_FRAME, self.onEnterFrame, self)	

	self.timer:start()
	self.timer2:start()
	self.timerCounter = 1
	self.soundFile = "sound/stad_.wav"
	self.ss = Sound.new(self.soundFile)
	if soundYes then 
		self.soundChannel1  = self.ss:play()
	end 
	
	--self.dots = {}

	self.gameStarting = true 
	c4 = os.timer() - c3
	print("--------------------------------------------------------- 2  ",  c4 )

end 	



function QuizGame:onEnterFrame(event)
	if self.gameStarting then 

		self.timerCounter = self.timerCounter + 1
		if self.timerCounter == 25 then 

			for i = 1, self.counter do
				local p = math.random(1,3)
				if p == 1 then 
					self.people[i][1]:setVisible(false)
					self.people[i][2]:setVisible(true)
					self.people[i][3]:setVisible(false)
					self.people[i][4]:setVisible(false)
				elseif p == 2 then 
					self.people[i][1]:setVisible(true)		
					self.people[i][2]:setVisible(false)
					self.people[i][3]:setVisible(false)
					self.people[i][4]:setVisible(false)	
				elseif p == 3 then 
					self.people[i][1]:setVisible(true)		
					self.people[i][2]:setVisible(false)
					self.people[i][3]:setVisible(false)	
					self.people[i][4]:setVisible(false)			
				else
					self.people[i][1]:setVisible(false)		
					self.people[i][2]:setVisible(false)
					self.people[i][3]:setVisible(false)	
					self.people[i][4]:setVisible(true)	
				end 
			end 

		end 
		
		if self.timerCounter > 25 then 
			self.timerCounter = 0
		end
		self.timerCounter = self.timerCounter + 1
	end 	
	
	if self.gameCheering then 
		
		self.timerCounter = self.timerCounter + 1
		if self.timerCounter == 20 then 		
	
			for i =1, #self.people do 
				for j = 1, #self.people[i] do
					self.people[i][j]:setVisible(false)	
				end 
				
				if self.people[i].counter  > self.people[i].cheer then
					self.people[i].counter = 1
				end 				
				self.people[i][self.people[i].counter]:setVisible(true)
				self.people[i].counter = self.people[i].counter  + 1

			end 
		end 	
		
		if self.timerCounter > 20 then 
			self.timerCounter = 0
		end
		self.timerCounter = self.timerCounter + 1		
	
	end 
	
	if self.gameWeeping then 
		self.timerCounter = self.timerCounter + 1
	
		if self.timerCounter == 20 then 		
	
			for i =1, #self.people do 
				for j = 1, #self.people[i] do
					self.people[i][j]:setVisible(false)	
				end 
				
				if self.people[i].counter  > #self.people[i] then
					self.people[i].counter = self.people[i].cheer + 1
				end 
				
				self.people[i][self.people[i].counter]:setVisible(true)
			
				self.people[i].counter = self.people[i].counter  + 1
				

			end 
		end 	
		
		if self.timerCounter > 20 then 
			self.timerCounter = 0
		end
		self.timerCounter = self.timerCounter + 1		
	
	
	end 
end 




function QuizGame:startLoadAnimation()
	self.startAnimationImages = {}
	
	self.startAnimationImages[1] = Bitmap.new(TextureRegion.new(Texture.new("gfx/numbers/1.png", true )))
	self.startAnimationImages[2] = Bitmap.new(TextureRegion.new(Texture.new("gfx/numbers/2.png", true )))
	self.startAnimationImages[3] = Bitmap.new(TextureRegion.new(Texture.new("gfx/numbers/3.png", true )))
	for i = 1,3 do
		self.startAnimationImages[i]:setScale(0.2, 0.2 )
		self.startAnimationImages[i]:setAnchorPoint(0.5, 0.5)
		self.startAnimationImages[i]:setPosition(X_HIGH/2, Y_HIGH/2-100 )
		self.qSprite:addChild(self.startAnimationImages[i])
	end 

	local scaleFactor = 2

	self.animTweens = {}
	self.animTweens[#self.animTweens + 1 ] = GTween.new(self.startAnimationImages[3], 0.5 ,{alpha = 1, scaleX = scaleFactor, scaleY = scaleFactor}, { ease = easing.outBack, delay = 0, repeatCount = 1, reflect = false } )				
	
	self.animTweens[#self.animTweens]:addEventListener("complete", function () 
		 self.startAnimationImages[3]:setVisible(false)
	end, self )
	self.animTweens[#self.animTweens].dispatchEvents = true 


	self.animTweens[#self.animTweens + 1 ] = GTween.new(self.startAnimationImages[2], 0.5,{ alpha = 1, scaleX = scaleFactor, scaleY = scaleFactor}, { ease = easing.outBack, delay = 1, repeatCount = 1, reflect = false } )				
	self.animTweens[#self.animTweens]:addEventListener("complete", function () 
		 self.startAnimationImages[2]:setVisible(false)
	end, self )
	self.animTweens[#self.animTweens].dispatchEvents = true 


	self.animTweens[#self.animTweens + 1 ] = GTween.new(self.startAnimationImages[1], 0.5, {alpha = 1, scaleX = scaleFactor, scaleY = scaleFactor}, { ease = easing.outBack, delay = 2, repeatCount = 1, reflect = false } )				
	self.animTweens[#self.animTweens]:addEventListener("complete", function () 
		 self.startAnimationImages[1]:setVisible(false)
--		self:addEventListener(Event.MOUSE_DOWN, self.onMouseDown, self)
--		self:addEventListener(Event.MOUSE_MOVE, self.onMouseMove, self)
--		
--		self:addEventListener(Event.MOUSE_UP, self.onMouseUp, self)
--		self:addEventListener(Event.ENTER_FRAME, self.onEnterFrame, self)	
--		self.mouseOn = true 		 
		
		
		self.soundFile2 = "sound/whis.mp3"
		self.ss2 = Sound.new(self.soundFile2)
		if soundYes then 
			self.soundChannel2  = self.ss2:play()
		end 	
		self:askNewQuestion()	
		self.timeCountdown:start() 
	end, self )
	self.animTweens[#self.animTweens].dispatchEvents = true 

			

end 

function QuizGame:updatePuan()
	if self.pointField then 
		if self.topScore:contains(self.pointField) then 
			self.topScore:removeChild(self.pointField)
		end 	
	end 

	self.pointField = TextField.new(fontSF, self.points)
	--self.pointField:setAnchorPoint(0.5,0.5)
	self.pointField:setPosition(950,300)
	self.pointField:setTextColor(0xeeee00)
	self.topScore:addChild(self.pointField)



end 


function QuizGame:setDot(reset)

	if self.trueCount > 3 then 
		self.points = self.points + self.trueCount * self.trueCount * 2
		self:updatePuan()
	end 

	if reset == true then 
		for i = 1,5 do
			self.dots[i]:setVisible(false)
			self.dots[i]:setAlpha(0)
		end 	
		self.trueCount = 0 
		return 
	end 

	self.trueCount = self.trueCount + 1
	if self.trueCount > 5 then 
		self.trueCount = 1
		for i = 1,5 do
			self.dots[i]:setVisible(false)
			self.dots[i]:setAlpha(0)
		end 
	end 
	
--	self.dots[self.trueCount] =  Bitmap.new(TextureRegion.new(Texture.new("gfx/dot2.png", true )))
--	self.topScore:addChild(self.dots[self.trueCount])
--	self.dots[self.trueCount]:setPosition(trueDotPosition[self.trueCount].x , trueDotPosition[self.trueCount].y)
	print("dots", self.dots, self.trueCount)
	
	self.dots[self.trueCount]:setVisible(true)
	self.ttt =  GTween.new(self.dots[self.trueCount], 0.2 ,  {alpha = 1},  { ease = easing.linear, delay = 0, repeatCount = 1, reflect = false } )
	self.topScoreTween2 =  GTween.new(self.topScore, 0.5 ,  {alpha = 1, x=0  , y = -100},  { ease = easing.linear, delay = 0, repeatCount = 1, reflect = false } )
end 

function QuizGame:dogruAnimation()
	if soundYes then 
		self.soundChannelD  = self.ssD:play()
	end 	
	self.points = self.points + 10 	
	self:updatePuan()
	
	for i=1,#self.people do 
		self.people[i].counter = math.random(1, self.people[i].cheer)
	end 
	local Q = self.questionList[self.questionCounter]

	self.twList = {}
	
	self.correct =  Bitmap.new(TextureRegion.new(Texture.new("gfx/check.png", true )))
	self.answers[Sorular[Q].dogru]:setColorTransform(1,1,0.4,1)
	self.answers[Sorular[Q].dogru]:addChild(self.correct)
	self.correct:setPosition(100,-100)

	for i =1,4 do 
		if i ~= Sorular[Q].dogru then 
			self.twList[i] = GTween.new(self.answers[i], 0.4 ,  {alpha = 0,  y = self.answers[i].y + 500},  { ease = easing.linear, delay = 1.5, repeatCount = 1, reflect = false } )
		end 	
	end 

	self.twList[Sorular[Q].dogru] = GTween.new(self.answers[Sorular[Q].dogru], 0.5 ,  {alpha = 0},  { ease = easing.linear, delay = 2, repeatCount = 1, reflect = false } )
	self.twList[Sorular[Q].dogru]:addEventListener("complete", 
				function () 
					self.qSprite:removeChild(self.questionImage)	
					for i = 1,4 do 
						self.qSprite:removeChild(self.answers[i])
					end 
				end, self )
	self.twList[Sorular[Q].dogru].dispatchEvents = true	


	self.twList[5] = GTween.new(self.questionImage, 0.5 ,  {alpha = 0,   y = -400},  { ease = easing.outBack, delay = 1, repeatCount = 1, reflect = false } )


--	self.questionImage:setVisible(false)
--	for i=1,4 do
--		self.answers[i]:setVisible(false)
--	end 
	
	self.timerTrue = Timer.new(3000, 1)
	self.timerTrue:addEventListener(Event.TIMER, self.onTimerTRUE, self)
	self.timerTrue:start()
	self.timerCounter = 1
	
	self.gameCheering = true 

	self:setDot()
end 

function QuizGame:onTimerTRUE()
	self.gameCheering = false 
	
	self:askNewQuestion()
end 

function QuizGame:onTimerFALSE()
	self.gameWeeping = false 
	
	self:askNewQuestion()
end 

function QuizGame:yanlisAnimation(falseItem)
	self.points = self.points - 10 	
	self:setDot(true)
	self:updatePuan()
	if soundYes then 
		self.soundChannelY  = self.ssY:play()
	end 
	
	for i=1,#self.people do 
		self.people[i].counter = math.random(self.people[i].cheer + 1, #self.people[i])
	--	print("yanlisanimation", self.people[i].cheer + 1, #self.people[i])
	end 
	local Q = self.questionList[self.questionCounter]

	self.twList = {}
	
	self.falseM =  Bitmap.new(TextureRegion.new(Texture.new("gfx/cross.png", true )))
	self.answers[falseItem]:setColorTransform(0.9,0.5,0.5,1)
	self.answers[falseItem]:addChild(self.falseM)
	self.falseM:setPosition(100,-100)
	
	for i =1,4 do 
		if i ~= falseItem then 
			self.twList[i] = GTween.new(self.answers[i], 0.4 ,  {alpha = 0,  y = self.answers[i].y + 500},  { ease = easing.linear, delay = 1.5, repeatCount = 1, reflect = false } )
		end 	
	end 

	self.twList[falseItem] = GTween.new(self.answers[falseItem], 0.5 ,  {alpha = 0},  { ease = easing.linear, delay = 2.0, repeatCount = 1, reflect = false } )
	self.twList[falseItem]:addEventListener("complete", 
				function () 
					self.qSprite:removeChild(self.questionImage)	
					for i = 1,4 do 
						self.qSprite:removeChild(self.answers[i])
					end 
				end, self )
	self.twList[falseItem].dispatchEvents = true					
	

	self.twList[5] = GTween.new(self.questionImage, 0.5 ,  {alpha = 0,   y = -400},  { ease = easing.linear, delay = 1, repeatCount = 1, reflect = false } )


--	self.questionImage:setVisible(false)
--	for i=1,4 do
--		self.answers[i]:setVisible(false)
--	end 
	
	self.timerFalse = Timer.new(3000, 1)
	self.timerFalse:addEventListener(Event.TIMER, self.onTimerFALSE, self)
	self.timerFalse:start()
	self.timerCounter = 1
	
	self.gameWeeping = true 	
	
end 

function QuizGame:onMouseDown(event)

	if self.gameCheering == true or self.gameWeeping == true then 
		return
	end 	

	local Q = self.questionList[self.questionCounter]
	print(Q)
	print("the question is", Q, "doğru", Sorular[Q].dogru)
	if self.answering then 
		for i = 1,4 do 
			if self.answers[i]:hitTestPoint(event.x, event.y) == true then 
				if i == Sorular[Q].dogru then  -- dogru 
					print("DOGRUUUUUUUU")
					self:dogruAnimation()		
					return  					
				else 
					print("YANLISSS", i)
					self:yanlisAnimation(i)
					event:stopPropagation()
					return  					
				end 
			end 
		end 	
	end 
	
	if self.answering then 
	
		if self.buttons[2]:hitTestPoint(event.x, event.y) == true then 
			self.buttons[2]:setVisible(false)
			
			self:setHalf()
		end 
		
		if self.buttons[4]:hitTestPoint(event.x, event.y) == true then 
			self.buttons[4]:setVisible(false)
			self:setSkip()
		end 
	end 	
end 

function QuizGame:setHalf()
	Q1 = self.questionList[self.questionCounter]
	list = {}
	for i = 1,4 do
		if  i ~=  Sorular[Q1].dogru then 
			list[#list + 1] =  i
		end 
	end 	

	for  i = 1,10 do
		p1=math.random(1,3)
		p2 =math.random(1,3)
		temp = list[p1]
		list[p1]= list[p2]
		list[p2] = temp
	end 
	
	self.tplist = {}
	for i =1,2 do 
		self.tplist[i] = GTween.new(self.answers[list[i]], 0.5 ,  {alpha = 0,  y = self.answers[list[i]].y + 500},  { ease = easing.linear, delay = 0, repeatCount = 1, reflect = false } )
	end 

end 

function QuizGame:setSkip()
	self:resetAnimation()
	
	
	for i =1,4 do 
		self.twList[i] = GTween.new(self.answers[i], 0.5 ,  {alpha = 0,  y = self.answers[i].y + 500},  { ease = easing.linear, delay = 0, repeatCount = 1, reflect = false } )
	end 

	self.twList[4]:addEventListener("complete", 
				function () 
					self.qSprite:removeChild(self.questionImage)	
					for i = 1,4 do 
						self.qSprite:removeChild(self.answers[i])
					end 
				end, self )
	self.twList[4].dispatchEvents = true					
	

	self.twList[5] = GTween.new(self.questionImage, 0.5 ,  {alpha = 0,   y = -400},  { ease = easing.linear, delay = 0, repeatCount = 1, reflect = false } )


	
	self.timerSkip= Timer.new(800, 1)
	self.timerSkip:addEventListener(Event.TIMER, self.onTimerSkip, self)
	self.timerSkip:start()
	self.timerCounter = 1
	
	--self.gameWeeping = true 		

end 


function QuizGame:onTimerSkip()

	self:askNewQuestion()
end 

function QuizGame:resetAnimation()

	for i = 1, #self.people do
		self.people[i][1]:setVisible(true)
		for j = 2, #self.people[i] do 
			self.people[i][j]:setVisible(false)
		end 
	end 	
			
end 

function QuizGame:askNewQuestion()
	self:resetAnimation()
	
	local questionNo = self.questionCounter 
	self.questionCounter = self.questionCounter + 1
	
	local Q = self.questionList[self.questionCounter]
	print(self.questionCounter, Q, Sorular[Q])

	self.questionImage = Bitmap.new(TextureRegion.new(Texture.new("gfx/soru2.png", true )))


	self.questionImage:setAnchorPoint(0.5, 0.5)
	

	
	xx = X_HIGH - self.questionImage:getWidth()
	--print(self:getWidth(), self.questionImage:getWidth() )
	
	self.midpoint = { x  = xx , y = 500 }
	--print("--->>>", X_HIGH,self.midpoint.x, self.midpoint.y )
	self.questionImage:setPosition(self.midpoint.x+500, self.midpoint.y -1500)
	
	

	self.topScore:setPosition(0,-200)
--	self.topScore:setAlpha(0)
	
	Sorular[Q].visited = true 
	
	self.text = {}
	for i =1, #Sorular[Q].soru do 
		self.text[i] = TextField.new(fontF, Sorular[Q].soru[i])
		self.questionImage:addChild(self.text[i])
		self.text[i]:setPosition(-480-200-20 , 0 + (i-1) * 100)
	end 	
	
	xx = xx - 512/2-10-20+200+160
	local pp =  300-10
	self.ansPos = {  {x  = xx, y = 850} , {x  = xx + 512+55+ pp, y = 850} , {x = xx, y = 1050}, {x = xx + 512+55+pp, y = 1000 +50}}

	
	self.answers = {}
	for i = 1,4 do 
		self.answers[i] = Bitmap.new(TextureRegion.new(Texture.new("gfx/cevap2.png", true )))
		self.answers[i]:setAnchorPoint(0.5, 0.5)
		self.answers[i].x = self.ansPos[i].x
		self.answers[i].y = self.ansPos[i].y
		self.answers[i]:setPosition(self.ansPos[i].x , self.ansPos[i].y )
		self.qSprite:addChild(self.answers[i])
	end 
	
	self.answers[1]:setPosition(self.ansPos[1].x-1000 , self.ansPos[1].y )
	self.answers[2]:setPosition(self.ansPos[2].x+1000 , self.ansPos[2].y )
	self.answers[3]:setPosition(self.ansPos[3].x-100 , self.ansPos[3].y+1000 )
	self.answers[4]:setPosition(self.ansPos[4].x+100 , self.ansPos[4].y +1000)


	self.atext = {}
	print("----SECENEKLER", "SORU", Q)
	for i =1, #Sorular[Q].secenek do 
		self.atext[i] = TextField.new(fontF2, Sorular[Q].secenek[i])
		print(i, Sorular[Q].secenek[i])
		self.answers[i]:addChild(self.atext[i])
		self.atext[i]:setPosition(-300 , 0 )
			self.answers[i]:setAlpha(0)
	end 	

	self.qSprite:addChild(self.questionImage)
	self:addChild(self.topScore)	

	self.topScoreTween =  GTween.new(self.topScore, 1 ,  {alpha = 1, x=0  , y = 0},  { ease = easing.outBounce, delay = 1, repeatCount = 1, reflect = false } )
	self.qImageTween =  GTween.new(self.questionImage, 1 ,  {alpha = 1, y = self.midpoint.y},  { ease = easing.outBounce, delay = 0, repeatCount = 1, reflect = false } )

	self:addChild(self.timePark)
	self.timeParkTween =  GTween.new(self.timePark, 1 ,  {alpha = 1, x=1800  , y = 100},  { ease = easing.outBounce, delay = 1, repeatCount = 1, reflect = false } )
	--self:finalCountdown()
	self.timeParkTween:addEventListener("complete", function() 
				--self.timePark:addChild(self.countdown[0])
			end, self )
	self.timeParkTween.dispatchEvents = true 

	self.twList = {}	
	
	
	for i = 1,4 do 
		self.twList[i] = GTween.new(self.answers[i], 0.5 ,  {alpha = 1,x =  self.answers[i].x,  y = self.answers[i].y},  { ease = easing.outBack, delay = 1 + 0.25 * i, repeatCount = 1, reflect = false } )	
	end 	
	self.answering = true 
end 


function QuizGame:loadCountdown()

	self.countdown  = {}
	for i =0, 59 do
		self.countdown[i] = TextField.new(fontSF, 60 - i)
		self.countdown[i]:setPosition(-100,40)
		self.countdown[i]:setTextColor(0xffff00)
		self.timePark:addChild(self.countdown[i])
		self.countdown[i]:setVisible(false )
	end 

	self.countdownCounter = 0
	self.countdown[self.countdownCounter ]:setVisible(true )

end

function QuizGame:finalCountdown()
	--print("------------------",self.countdownCounter, self.countdown[self.countdownCounter])
	self.countdown[self.countdownCounter ]:setVisible(false )
	
	self.countdownCounter = self.countdownCounter + 1
	--print("--***************---",self.countdownCounter, self.countdown[self.countdownCounter])
	
	if self.countdownCounter < 60 then 
			self.countdown[self.countdownCounter]:setVisible(true)
			
		else
			
			self.soundFileM = "sound/sampiyon.wav"
			self.sss = Sound.new(self.soundFileM)
			if soundYes then 
				self.soundChannel1s  = self.sss:play()	
			end 
			ev= Event.new("hebe")
			ev.page = 3
			self.manager:gotoPage(ev)
		end 

end 

function QuizGame:onTimer2(event)
	if self.soundChannel1 then 
		self.soundChannel1:setPaused(true)
	end 
	self.gameStarting = false 
	self:startLoadAnimation()



	print("animasyon bitti")
 end


function QuizGame:onTimer(event)
	self.people[self.randList[self.counter]][1]:setVisible(true)
	self.counter = self.counter + 1
	if self.counter > #self.people then 
		self.counter = #self.people 
	--	self.soundChannel1:setPaused(true)
	--	self.gameCheering = true 
	end 
 end

function QuizGame:loadBK()

	self.qSprite.bk = Bitmap.new(TextureRegion.new(Texture.new(back, true )))
--	self.qSprite.bk:setAlpha(0.1)

	self.m2 = Bitmap.new(TextureRegion.new(Texture.new("gfx/bos1.png", true )))
	self.m3 = Bitmap.new(TextureRegion.new(Texture.new("gfx/bos1.png", true )))


--	self.qSprite.bk:setColorTransform(0.1,0.1,1,0.5)
--	self.m2:setColorTransform(1,0.5,0.5,0.5)
--	self.m3:setColorTransform(1,1,0.5,0.5)
	
t = -8
	self.m2:setPosition(-400+5+5+t,0)
	self.m3:setPosition(2048-5-5-t-1,0)
	

	local xx = (X_HIGH - self.qSprite.bk:getWidth())  / 2
	local yy = 0
	self.qSprite.bk:setPosition(xx,yy)
	self.xx = xx
	self.yy = yy


	self:addChild(self.m2)
	self:addChild(self.m3)
	self:addChild(self.qSprite.bk)	
	
	print("------------------------------basta", self:getWidth())
	
	self.topScore = Bitmap.new(TextureRegion.new(Texture.new("gfx/Stad_Scoreboard.png", true )))
	
	self.emptySeat = Bitmap.new(TextureRegion.new(Texture.new("gfx/emptyseats.png", true )))
	self:addChild(self.emptySeat)
	self.emptySeat:setPosition(459,176)	
	

	self.seats = {}
	self.people = {}
	for i=1,6 do
		self.seats[i] = Bitmap.new(TextureRegion.new(Texture.new(seatList[i], true )))
		self:addChild(self.seats[i])
		self.seats[i]:setPosition(seatPositionList[i].x, seatPositionList[i].y)
		for j = 1,7 do
			if peopleCoordList[i][j] > 0 then 
				local person = peopleCoordList[i][j]
				self.people[#self.people + 1] = {}
				local index = #self.people
				--print(index)
				for k = 1, #peopleList[person] do
					--print(peopleList[person][k])
					--self.people[index][k] = Bitmap.new(TextureRegion.new(Texture.new(peopleList[person][k], true )))
					self.people[index][k] = fanPeople[index][k]
					local image = self.people[index][k]
					self:addChild(image)
					image:setPosition(positionList[i][j].x, positionList[i][j].y)
					self.people[index].cheer = peopleList[person].cheer 
					if k > 0 then image:setVisible(false) end 
				end 
			end 	
		end 
	
	end 
	
	print("------------------------------basta", self:getWidth())


	self.bottom = Bitmap.new(TextureRegion.new(Texture.new("gfx/bott01.png", true )))
	self:addChild(self.bottom)
	self.bottom:setPosition(-400,Y_HIGH - 400)	

	self.bottom2 = Bitmap.new(TextureRegion.new(Texture.new("gfx/bot02.png", true )))
	self:addChild(self.bottom2)
	self.bottom2:setPosition(2048-400-40,Y_HIGH - 400)	
	
		print("------------------------------basta", self:getWidth())
	self:addChild(self.qSprite)
	self.dots= {}
	
	for  i =1,5 do
		self.dots[i] =  Bitmap.new(TextureRegion.new(Texture.new("gfx/dot2.png", true )))
		self.topScore:addChild(self.dots[i])
		self.dots[i]:setPosition(trueDotPosition[i].x , trueDotPosition[i].y)
		self.dots[i]:setVisible(false)
		self.dots[i]:setAlpha(0)
	end 	
	print("loaddaki",self.dots)
	
	self.timePark =  Bitmap.new(TextureRegion.new(Texture.new("gfx/tim_.png", true )))
	self.timePark:setAnchorPoint(0.5, 0.5)
	self.timePark:setPosition(2048,-50)
	self:addChild(self.timePark)
	self:loadCountdown()
	self:loadButtons()

	--self:loadCountdown()


end 	




function QuizGame:loadButtons()

	self.buttons = {}
	self.buttons[1] = Bitmap.new(TextureRegion.new(Texture.new("gfx/icons/pyuzde50_01.png", true )))
	self.buttons[2] = Bitmap.new(TextureRegion.new(Texture.new("gfx/icons/pyuzde50_02.png", true )))

	
	self.buttons[3] = Bitmap.new(TextureRegion.new(Texture.new("gfx/icons/m2.png", true )))
	self.buttons[4] = Bitmap.new(TextureRegion.new(Texture.new("gfx/icons/m1.png", true )))

	for i = 1, 4 do
		self.buttons[i]:setAnchorPoint(0.5, 0.5)
		self.bottom2:addChild(self.buttons[i])

	end 

	self.buttons[1]:setPosition(-100, 250)
	self.buttons[2]:setPosition(-100, 250)
	self.buttons[3]:setPosition(200, 250)
	self.buttons[4]:setPosition(200, 250)
	
	

end 