
Sorular = {}

for i = 1,105 do
	Sorular[i] = {}
	Sorular[i].visited = false
end 

Sorular[1].soru={"Fenerbahçe Spor Kulübünün ilk renkleri ","aşağıdakilerden hangisidir?"}
Sorular[2].soru={"Fenerbahçe’nin günümüzde kullanılan ","armasını kim çizmiştir? "}
Sorular[3].soru={"Fenerbahçe’nin 1959’dan beri düzenlenen ","Süper ligde kaç şampiyonluğu vardır?"}
Sorular[4].soru={"Fenerbahçe kaç kez Türkiye Kupasını ","kazanmıştır?"}
Sorular[5].soru={"Fenerbahçe tarihinde adı soyadı aynı olan ","iki futbolcu kimlerdir?"}
Sorular[6].soru={"Fenerbahçe hangi iki takımı ilk yarısını 3 - 0 geride ","bitirdiği maçlarda 4-3 yenmiştir?"}
Sorular[7].soru={"Fenerbahçe’nin 103 golle şampiyon olduğu 1988–1989","sezonunda 100. golünü hangi futbolcu atmıştır?"}
Sorular[8].soru={"Fenerbahçe lig tarihinde kaç tane gol kralı çıkarmıştır?",""}
Sorular[9].soru={"Aşağıdaki futbolculardan hangisi hem Fenerbahçe’de"," hem de Beşiktaş’ta forma giymemiştir?"}
Sorular[10].soru={"Aşağıdakilerden hangisi Fenerbahçe’nin"," forma sponsoru olmamıştır?"}
Sorular[11].soru={"Fenerbahçe’nin  deplasmanlı basketbol ","liginde kaç şampiyonluğu vardır?"}
Sorular[12].soru={"Hangisi hem Fenerbahçe’de hem de Galatasaray’da"," forma giymemiştir?"}
Sorular[13].soru={"Aşağıdaki isimlerden hangisi Fenerbahçe’nin ","ilk üç kurucusundan biri değildir?"}
Sorular[14].soru={"Fenerbahçe’nin Türkiye liginde en çok yendiği ","Anadolu takımı hangisidir.?"}
Sorular[15].soru={"Hangisİ Fenerbahçe formasıyla gol kralı olmamıştır?",""}
Sorular[16].soru={"Hangisi Fenerbahçe’de teknik direktörlük ","yapmamıştır? "}
Sorular[17].soru={"1989 -1991 arası Fenerbahçe’de forma giyen Şenol Üç  ","lakablı futbolcunun soyadı nedir?"}
Sorular[18].soru={"Fenerbahçe tarihinde kaç kez yerli antrenörle ","şampiyonluğa ulaşmıştır?"}
Sorular[19].soru={"Fenerbahçe tarihindeki ilk maçını ","hangi takıma karşı oynamıştır?"}
Sorular[20].soru={"Mustafa Kemal Atatürk hangi yıl Fenerbahçe Kulübünü ","ziyaret etti?"}
Sorular[21].soru={"1910 yılında hangi semt kulübü kendini ","fesih edip Fenerbahçe’ye katılmıştır?"}
Sorular[22].soru={"Hangisi hem futbolcu hem de antrenör olarak"," şampiyonluk yaşamıştır?"}
Sorular[23].soru={"Fenerbahçe Spor Kulübü armasındaki palamut yaprağı ","neyi temsil etmektedir?"}
Sorular[24].soru={"Fenerbahçe kuruluşundan itibaren toplamda kaç ","Türkiye şampiyonluğu kazanmıştır?"}
Sorular[25].soru={"Aşağıdaki teknik direktörlerden hangisi ile"," Fenerbahçe şampiyon olmamıştır?"}

Sorular[26].soru={"Fenerbahçe’nin hem futbol, hem de basketbol","takımında oynamış oyuncusu hangisidir?"}

Sorular[27].soru={"Yurt dışına kulubüne para kazandırarak giden"," ilk futbolcu kimdir?"}
Sorular[28].soru={"Fenerbahçe tarihinde en uzun süre başkanlık ","yapan kişi kimdir?"}
Sorular[29].soru={"Hangisi Fenerbahçe’de teknik direktörlük"," antrenörlük yapmamıştır?"}
Sorular[30].soru={"İşgal yıllarında hangi İngiliz komutan  adına ","düzenlenen kupayı Fenerbahçe kazanıştır?"}
Sorular[31].soru={"Hangi yabancı ulustan antrenörle Fenerbahçe ","hiç  şampiyon olmamıştır?"}
Sorular[32].soru={"Fenerbahçe tarihinin en çok forma giyen ","futbolcusu hangisidir?"}
Sorular[33].soru={"Aşağıdaki futbolculardan hangisi jübilesinde ","gol atmıştır?"}
Sorular[34].soru={"Aşağıdaki oyunculardan  hangisi Fenerbahçe’den"," yurt dışına transfer olmamıştır?"}
Sorular[35].soru={"Fenerbahçe  1969 -1970 sezonunda  kaç gol yemiştir?",""}
Sorular[36].soru={"Fenerbahçe 1914 yılında Galatasaray’a karşı aldığı"," ilk galibiyetin skoru nedir?"}
Sorular[37].soru={"1959’dan itibaren Fenerbahçe’deki kariyerlerinde ","en çok lig şampiyonluğu gören futbolcular hangileridir?"}
Sorular[38].soru={"Fenerbahçe’nin Ligdeki 1000. Golünü kim atmıştır?",""}
Sorular[39].soru={"Fenerbahçe’nin Ligdeki 2000. Golünü kim atmıştır?",""}
Sorular[40].soru={"Fenerbahçe’nin Ligdeki 3000. Golünü kim atmıştır?",""}
Sorular[41].soru={"Mehmetçik lakaplı futbolcu kimdir?",""}
Sorular[42].soru={"Fenerbahçe ilki 1992 de yapılan Şampiyonlar Ligine ","ilk kez hangi sezon katılmıştır?"}
Sorular[43].soru={"Fenerbahçe Kulübü bugün kaç branşta ","faaliyet göstermektedir?"}
Sorular[44].soru={"Fenerbahçe futbolda Şampiyonlar Ligine toplam ","kaç kez katılmıştır?"}
Sorular[45].soru={"Bayan Basketbol takımımız ilk kez hangi sezon ","lig şampiyonu olmuştur?"}
Sorular[46].soru={"Erkek Basketbol takımımız ilk kez hangi yıl lig ","şampiyonu  olmuştur?"}
Sorular[47].soru={"Fenerbahçe Cumhurbaşkanlığı ","Süper Kupa’yı kaç kez kazandı?"}
Sorular[48].soru={"Fenerbahçe Başbakanlık Kupasını kaç kez kazandı? ",""}
Sorular[49].soru={"Fenerbahçe TSYD Kupasını kaç kez kazandı? ",""}
Sorular[50].soru={"Fenerbahçe 2012 2013 sezonunda UEFA Kupasında ","yarı finale çıkarken hangi takımla oynamadı?"}


Sorular[51].soru={"Futbolda en çok forma giyen"," yabancı futbolcu kimdir?"}
Sorular[52].soru={"Rıdvan Dilmen jübilesinde Fenerbahçe ","hangi  takımla oynamıştır?"}
Sorular[53].soru={"Arap lakaplı futbolcumuz hangisidir?",""}
Sorular[54].soru={"Okacha Fenerbahçe’ye hangi  Alman ","takımından transfer olmuştur?"}
Sorular[55].soru={"Uçan Kaleci lakaplı kalecimiz kimdir?",""}
Sorular[56].soru={"Galatasaray’a en çok gol atan ","Fenerbahçeli futbolcu kimdir?"}
Sorular[57].soru={"Fenerbahçe Avrupa kupalarında hangi takımın kendi","sahasındaki 40 yıllık yenilmezliğine son vermiştir?"}
Sorular[58].soru={"Kadın Basketbol takımımızın ligde ","toplamda kaç şampiyonluğu vardır?"}
Sorular[59].soru={"Kadın Voleybol  takımımızın ligde  ","toplamda kaç şampiyonluğu vardır?"}
Sorular[60].soru={"Erkek Voleybol takımımızın ligde ","toplamda kaç şampiyonluğu vardır?"}
Sorular[61].soru={"18 Eylül 1985 tarihindeki Bordeaux maçında ","hangi futbolcu gol atamamıştır?"}
Sorular[62].soru={"06 Kasım 2002 tarihindeki Galatasaray"," maçında hangi futbolcu gol atamamıştır?"}
Sorular[63].soru={"Galatasaray’dan  Fenerbahçe’ye transfer ","olan ilk futbolcu kimdir?"}
Sorular[64].soru={"Galatasaray’a tarihimizdeki ilk golü"," hangi futbolcu atmıştır?"}
Sorular[65].soru={"Fenerbahçe Avrupa kupalarında hangi ","Avrupa takımıyla hiç maç yapmamıştır?"}
Sorular[66].soru={"Fenerbahçe’nin Türkiye liginde en gollü"," maç hangi takıma karşıdır?"}
Sorular[67].soru={"Türkiye Ligi tarihinde bir maçta en çok gol atan (6) ","Fenerbahçeli  futbolcu kimdir?"}
Sorular[68].soru={"Fenerbahçe Balkan Kupasını hangi ","yıl kazamıştır?"}
Sorular[69].soru={"Fenerbahçe tarihinde en kısa süre"," başkanlık yapan kişi kimdir?"}
Sorular[70].soru={"Türkiye Birinci liginde bir maçta 4","penaltı golü atan futbolcumuz kimdir?"}
Sorular[71].soru={"Fenerbahçe tarihinde  en uzun süre ","antrenörlük yapan kişi kimdir?"}
Sorular[72].soru={"Turkiye 1.liginde en cok forma giyen ","Fenerbahceli oyuncu  kimdir?"}
Sorular[73].soru={"Hangisi Fenerbahçe'de hem futbolcu","hem başkanlık yapmıştır?"}
Sorular[74].soru={"Fenerbahçe’nin boksta toplam ","kaç şampiyonluğu bulunmaktadır?"}
Sorular[75].soru={"Beşiktaş’a en çok gol atan Fenerbahçe'li ","futbolcu  kimdir?"}
Sorular[76].soru={"Aşağıdaki kalecilerden hangisi hem Beşiktaş","hem de Fenerbahçe’de oynamamistir?"}
Sorular[77].soru={"Hem Fenerbahçe başkanlığı, ","hem TC Başbakanlığı yapmış kişi kimdir?"}
Sorular[78].soru={"2000-2001 sezonundaki tarihi  4 -3 luk Gaziantep","maçında hattrick yapan futbolcumuz kimdir?"}
Sorular[79].soru={"Olimpiyatlarda atletizmde madalya alan ","ilk Fenerbahçe'li sporcu kimdir?"}
Sorular[80].soru={"Şampiyonlar ligi şampiyonluğuna sahip","iki şube hangileridir?"}
Sorular[81].soru={"103 gollu rekor şampiyonluk hangi sezon ","alinmistir?"}
Sorular[82].soru={"Fenerbahçe ile  1. Ligde şampiyonluk yasayan","ilk Brezilyali teknik direktörümüz kimdir?"}
Sorular[83].soru={"Fenerbahçe’nin kürekte toplam kaç","şampiyonluğu bulunmaktadır?"}
Sorular[84].soru={"Hangisi ayrı dönemlerde Fenerbahçe'de hem kalecilik"," hem teknik direktörlük yapmıştır?"}
Sorular[85].soru={"Fenerbahçe’nin Yugoslav kökenli oyuncularindan ","değildir?"}
Sorular[86].soru={"Hangi futbolcu Fenerbahçe kariyerini Galatasaray’a","gol atmadan tamamlamıstır?"}
Sorular[87].soru={"Fenerbahçe formasında en uzun forma"," sponsoru hangi kurum olmustur?"}
Sorular[88].soru={"Hangisi formamızı bugüne kadar imal eden","markalardan biri değildir?"}
Sorular[89].soru={"Sinyor lakaplı futbolcumuz kimdir?",""}
Sorular[90].soru={"1965 te yıkıldıktan sonra Fenerbahçe stadı ","hangi yıl tekrar kullanılmaya başlandı?"}
Sorular[91].soru={"Aşağıdaki takımlardan hangisine ligde Fenerbahçe","bir maçta 8 gol atmamistir?"}
Sorular[92].soru={"1988 1989 sezonunda Aykut Turan Oğuz dışında","Fenerbahçe’ye gelen 4. Sakaryaspor'lu oyuncu kimdir?"}
Sorular[93].soru={"Aşağıdaki takımlardan hangisine ligde ","Fenerbahçe bir maçta 6 gol atamamistir?"}
Sorular[94].soru={"Hangi Amerikalı basketbolcu Fenerbahçe’de ","oynamamıştır?"}
Sorular[95].soru={"Kariyerinde Dünya Kupası finali gören oynayan ","tek Fenerbahçe'li futbolcu kimdir?"}
Sorular[96].soru={"1981'de Avrupa karmasına seçilen Fenerbahçe’li ","basketbolcu kimdir?"}
Sorular[97].soru={"Basketbolda Yunanistan’a transfer olan ilk ","Fenerbahçe'li oyuncu kimdir?"}
Sorular[98].soru={"Hangisi Fenerbahce’de atletizm yapmış ","atletlerden değildir?"}
Sorular[99].soru={"Aşağıdaki Fenerbahçe'li sporculardan hangisi","Olimpiyatlarda madalya kazanmamıştır?"}
Sorular[100].soru={"Armasında olduğu halde hangi renk Fenerbahçe ","formasında hakim renk olarak hiç kullanılmamıştır?"}
Sorular[101].soru={"Şeytan lakaplı oyuncumuz kimdir?",""}
Sorular[102].soru={"İmparator lakaplı oyuncu kimdir?",""}
Sorular[103].soru={"Asker lakaplı gol kralı oyuncumuz kimdir?",""}
Sorular[104].soru={"Rambo lakaplı oyuncularimiz kimlerdir?",""}
Sorular[105].soru={"Miço lakaplı futbolcumuz kimdir?",""}













Sorular[1].secenek	=	{"Sarı - Lacivert","Sarı - Siyah","Sarı - Beyaz","Sarı – Mavi"}
Sorular[2].secenek={"Zeki Rıza Sporel","Hikmet Topuz","Sait Selahattin Cihanoğlu","Galip Kulaksızoğlu"}
Sorular[3].secenek={"20","18","19","17"}
Sorular[4].secenek={"5","4","8","6"}
Sorular[5].secenek={"Şenol Birol","Birol Pekel","Müjdat Yetkiner","Ahmet Erol "}
Sorular[6].secenek={"Galatasaray - Altay ","G. Antep - Beşiktaş","Galatasaray-G. Antep","Galatasaray - Adanaspor"}
Sorular[7].secenek={"Rıdvan Dilmen","Oğuz Çetin","Turhan Sofuoğlu","Aykut Kocaman"}
Sorular[8].secenek={"13","14","15","16"}
Sorular[9].secenek={"Şenol Birol","Nezihi Tosuncuk","Selim Soydan ","Erol Bulut "}
Sorular[10].secenek={"Emlak Bankası","Vakıfbank","İstanbul Bankası ","Akbank "}
Sorular[11].secenek={"5","6","7","8"}
Sorular[12].secenek={"Raşit Çetiner","Erhan Önal","Erdal Keser ","Mustafa Yücedağ "}
Sorular[13].secenek={"Ziya Songülen","Necip Okaner","Elkatipzade Mustafa","Ayetullah Bey "}
Sorular[14].secenek={"Ankaragücü","Antalya","Altay ","Eskişehirspor "}
Sorular[15].secenek={"Cemil Turan","Selçuk Yula ","Rıdvan Dilmen","Bülent Uygun "}
Sorular[16].secenek={"Yılmaz Yücetürk","Dorde Miliç","Kalman Mezsöly","Paul Csernai"}
Sorular[17].secenek={"Çorlu","Ustaömer","Ulusavaş","Birol "}
Sorular[18].secenek={"2","3","4","5"}
Sorular[19].secenek={"Moda FC","Kadıköy FC","Vefa","Galatasaray"}
Sorular[20].secenek={"1917","1918","1919","1920"}
Sorular[21].secenek={"Üsküdar","Kuşdili","Anadolu","Kadıköy "}
Sorular[22].secenek={"Mustafa Denizli","Radomir Antiç","Aykut Kocaman","Todor Veselinoviç"}
Sorular[23].secenek={"Asalet ","Kudret","Adalet","Sadakat"}
Sorular[24].secenek={"19","20","26","28"}
Sorular[25].secenek={"Tomislav Kaloperoviç","Branco Stankoviç","Ignace Molnar","Tomislav İviç "}
Sorular[26].secenek={"Lefter Küçükandonyadis","Can Bartu","Cihat Arman ","Fikret Kırcan"}
Sorular[27].secenek={"Lefter Küçükandonyadis","Can Bartu ","Engin Verel","Ogün Altıparmak"}
Sorular[28].secenek={"Şükrü Saracoğu","Ali Şen","Aziz Yıldırm","Faruk Ilgaz"}
Sorular[29].secenek={"Turhan Sofuoğlu","Oğuz Çetin","Erdoğan Arıca ","Rıdvan Dilmen"}
Sorular[30].secenek={"Amiral Lenington","General Harrington ","Churchill","Hiçbiri"}
Sorular[31].secenek={"Macar","İngiliz","Romanya ","Hollanda"}
Sorular[32].secenek={"Lefter Küçükandonyadis","Şeref Has","Cem Pamiroğlu","Müjdat Yetkiner"}
Sorular[33].secenek={"Lefter Küçükandonyadis","Ogün Altıparmak","Rıdvan Dilmen ","Şenol Çorlu"}
Sorular[34].secenek={"Lefter Küçükandonyadis","Can Bartu","Şeref Has","Ogün Altıparmak"}
Sorular[35].secenek={"6","17","21","23"}
Sorular[36].secenek={"1-0 ","3-1","4-2","2-1"}
Sorular[37].secenek={"Volkan Demirel Selçuk Şahin Semih Şentürk ","Alex Ümit Özat Rüştü Reçber","Selçuk Yula Yaşar Duran Erdoğan Arıca ","Can Bartu Lefter Cihat Arman "}
Sorular[38].secenek={"Erol Togay","Erdoğan Arıca ","Bahtiyar Yorulmaz","Ömer Kaner"}
Sorular[39].secenek={"Elvir Boliç","Moldovan","Kostadinov ","Uche "}
Sorular[40].secenek={"Deivid","Guiza","Alex","Lugano"}
Sorular[41].secenek={"Naci Erdem","Naci Bastoncu","Basri Dirimlili","Mehmet Ali Has"}
Sorular[42].secenek={"1992 – 1993","1993 – 1994","1995 – 1996 ","1996 – 1997 "}
Sorular[43].secenek={"7","8","9","10"}
Sorular[44].secenek={"4","5","6","7"}
Sorular[45].secenek={"1998 - 1999","1999 -2000","2000 – 2001","2001 – 2002"}
Sorular[46].secenek={"1990 - 1991","1991 – 1992","1992 -1993","1993 – 1994"}
Sorular[47].secenek={"9","10","11","12"}
Sorular[48].secenek={"7","8","9","10"}
Sorular[49].secenek={"10","11","12","13"}
Sorular[50].secenek={"Lazio","Bate Borisov","St. Etienne","Victoria Plzen"}
Sorular[51].secenek={"Uche","Dusan Pesiç ","Alex de Souza ","Lugano"}
Sorular[52].secenek={"Galatasaray","Bursaspor","Beşiktaş ","Ankaragücü"}
Sorular[53].secenek={"Gerson","Uche","İsmail Kartal","Arif Kocabıyık"}
Sorular[54].secenek={"Bayer Uerdingen","Eintracht Frankfurt","Werder Bremen ","Honnever 96"}
Sorular[55].secenek={"Hazım Cantez","Bedii Yazıcı","Cihat Arman","Erdal Kocaçimen"}
Sorular[56].secenek={"Alex","Aykut Kocaman","Zeki Rıza Sporel","Lefter"}
Sorular[57].secenek={"Juventus","Chelsea","Manchester United","AC Milan"}
Sorular[58].secenek={"10","11","12","13"}
Sorular[59].secenek={"2","3","4","5"}
Sorular[60].secenek={"3","4","5","6"}
Sorular[61].secenek={"Hüseyin Çakıroğlu","Şenol Çorlu","Selçuk Yula ","Cem Pamiroğlu"}
Sorular[62].secenek={"Ceyhun Eriş","Ümit Özat","Ortega","Steviç "}
Sorular[63].secenek={"Rebii Erkal ","İsmet Uluğ ","Hasan Kamil Sporel ","Bülent Varol"}
Sorular[64].secenek={"Zeki Rıza Sporel ","Galip Kulaksızoğlu","Hasan Kamil Sporel","Sait Selahattin Cihaoğlu"}
Sorular[65].secenek={"Göteborg","Maccabi Tel Aviv","Glosgow Rangers","Liverpool "}
Sorular[66].secenek={"Samsunspor","Kayserispor","Gaziantep","Eskişehirspor"}
Sorular[67].secenek={"Cemil Turan","Selçuk Yula","Tanju Çolak","Zafer Tüzün"}
Sorular[68].secenek={"1965 -1966 ","1966 - 1967","1967 - 1968","1968 -1969"}
Sorular[69].secenek={"Fikret Kırcan","Hasan Özaydın","Emin Cankurtaran ","Faruk Ilgaz "}
Sorular[70].secenek={"Selçuk Yula ","Aykut Kocaman ","Elvir Boliç ","Zafer Tüzün "}
Sorular[71].secenek={"Todor Veselinoviç","Ignace Molnar ","Christophe Daum ","Didi"}
Sorular[72].secenek={"Onur Kayador ","Müjdat Yetkiner ","Oguz Cetin ","Cem Pamiroglu "}
Sorular[73].secenek={"Ayetullah Bey ","Ziya Songulen ","Fikret Kircan ","Ismet Ulug"}
Sorular[74].secenek={"13","14","15","16"}
Sorular[75].secenek={"Naci Bastoncu ","Melih Katanca","Cemil Turan ","Moussa Sow "}
Sorular[76].secenek={"Engin İpekoğlu","Rüştü Reçber","Adem İbrahimoğlu ","Zafer Öger "}
Sorular[77].secenek={"İsmet Uluğ ","Şükrü Saraçoğlu","Faruk Ilgaz ","Emin Cankurtaran "}
Sorular[78].secenek={"Revivo ","Balic","Andersson ","Rapaic "}
Sorular[79].secenek={"Ruhi Sarialp ","Mehmet Terzi ","Elvan Abeylegese","Esref Apak "}
Sorular[80].secenek={"Bayan Basket Futbol","Bayan Voleybol Kurek ","Masa tenisi Bayan Voleybol ","Yüzme Boks"}
Sorular[81].secenek={"1984 1985 ","1988 1989","1982 1983","1987 1988"}
Sorular[82].secenek={"Parreira ","Zico ","Didi ","Lazaroni "}
Sorular[83].secenek={"33","34","35","36"}
Sorular[84].secenek={"Datcu ","Engin Ipekoglu ","Ivancevic ","Adem Ibrahimoglu "}
Sorular[85].secenek={"Lukovcan","Repcic ","Datcu ","Pesic"}
Sorular[86].secenek={"Ziegler","Edu","Tayfun ","Uche "}
Sorular[87].secenek={"Vakıf Bank ","Turk Bank ","Avea ","Emlak Bankası "}
Sorular[88].secenek={"Adidas","Gürer","Fenerıum","Umbro"}
Sorular[89].secenek={"Lefter Küçükandonyadis","Can Bartu ","Seref Has","Ogun Altiparmak "}
Sorular[90].secenek={"1979","1980","1982","1983"}
Sorular[91].secenek={"Kayseri ","Samsun ","Antep ","Eskişehir"}
Sorular[92].secenek={"İlker ","Bulent ","Serdar","Engin"}
Sorular[93].secenek={"Adana Demir ","Galatasaray ","Genclerbirligi ","Altay "}
Sorular[94].secenek={"Pete Williams","Conrad Mc Rea ","Mitch Smith ","Charles Dawkins "}
Sorular[95].secenek={"Jav Jay Okocha","Nicholas Anelka","Tony Schumacher","Ariel Ortega"}
Sorular[96].secenek={"Halil Dagli ","Engin Domanic","Efe Aydan ","Arif Derbent"}
Sorular[97].secenek={"Ali Limoncuoglu ","Guray Kanan ","Levent Topasakal","Ibrahim Kutluay "}
Sorular[98].secenek={"Mehmet Yurdadon ","Mehmet Terzi","Cengiz Kavaklioglu ","Bekir Karayel"}
Sorular[99].secenek={"Esref Apak","Eyüp Can","Gamze Bulut ","Malik Beyleroğlu "}
Sorular[100].secenek={"Sarı ","Beyaz ","Yeşil ","Kırmızı"}
Sorular[101].secenek={"Oğuz ","Aykut ","Rıdvan ","Turan "}
Sorular[102].secenek={"Rıdvan ","Oğuz ","Aykut ","Boliç"}
Sorular[103].secenek={"Basri","Selçuk","Bülent ","Aykut"}
Sorular[104].secenek={"Oğuz &Aykut","Pesic& Repcic ","Turan& Hasan ","Bolic& Balic "}
Sorular[105].secenek={"İsmail Kartal ","Müjdat Yetkiner","Nezihi Tosuncuk ","Turan Sofuoğlu "}


Sorular[	1	].dogru	=	3
Sorular[	2	].dogru	=	2
Sorular[	3	].dogru	=	3
Sorular[	4	].dogru	=	4
Sorular[	5	].dogru	=	3
Sorular[	6	].dogru	=	3
Sorular[	7	].dogru	=	3
Sorular[	8	].dogru	=	3
Sorular[	9	].dogru	=	4
Sorular[	10	].dogru	=	4

Sorular[	11	].dogru	=	2
Sorular[	12	].dogru	=	3
Sorular[	13	].dogru	=	3
Sorular[	14	].dogru	=	1
Sorular[	15	].dogru	=	3



Sorular[	16	].dogru	=	2
Sorular[	17	].dogru	=	3
Sorular[	18	].dogru	=	2
Sorular[	19	].dogru	=	2
Sorular[	20	].dogru	=	2
Sorular[	21	].dogru	=	2
Sorular[	22	].dogru	=	3

Sorular[	23	].dogru	=	4
Sorular[	24	].dogru	=	4
Sorular[	25	].dogru	=	4
Sorular[	26	].dogru	=	2
Sorular[	27	].dogru	=	1
Sorular[	28	].dogru	=	3
Sorular[	29	].dogru	=	3
Sorular[	30	].dogru	=	2




Sorular[	31	].dogru	=	4
Sorular[	32	].dogru	=	4
Sorular[	33	].dogru	=	3
Sorular[	34	].dogru	=	3
Sorular[	35	].dogru	=	1
Sorular[	36	].dogru	=	3
Sorular[	37	].dogru	=	1
Sorular[	38	].dogru	=	1
Sorular[	39	].dogru	=	4
Sorular[	40	].dogru	=	3
Sorular[	41	].dogru	=	3
Sorular[	42	].dogru	=	4
Sorular[	43	].dogru	=	2
Sorular[	44	].dogru	=	3
Sorular[	45	].dogru	=	1

Sorular[	46	].dogru	=	1
Sorular[	47	].dogru	=	1
Sorular[	48	].dogru	=	2
Sorular[	49	].dogru	=	3
Sorular[	50	].dogru	=	3

Sorular[	50	].dogru	=	3
Sorular[	51	].dogru	=	3
Sorular[	52	].dogru	=	2
Sorular[	53	].dogru	=	3
Sorular[	54	].dogru	=	2
Sorular[	55	].dogru	=	3


Sorular[	56	].dogru	=	3
Sorular[	57	].dogru	=	3
Sorular[	58	].dogru	=	2
Sorular[	59	].dogru	=	2
Sorular[	60	].dogru	=	2
Sorular[	61	].dogru	=	4
Sorular[	62	].dogru	=	4
Sorular[	63	].dogru	=	3
Sorular[	64	].dogru	=	3
Sorular[	65	].dogru	=	4
Sorular[	66	].dogru	=	3
Sorular[	67	].dogru	=	3
Sorular[	68	].dogru	=	2
Sorular[	69	].dogru	=	2
Sorular[	70	].dogru	=	4

Sorular[	71	].dogru	=	3
Sorular[	72	].dogru	=	3
Sorular[	73	].dogru	=	4
Sorular[	74	].dogru	=	2
Sorular[	75	].dogru	=	3


Sorular[	76	].dogru	=	4
Sorular[	77	].dogru	=	2
Sorular[	78	].dogru	=	4
Sorular[	79	].dogru	=	1
Sorular[	80	].dogru	=	3
Sorular[	81	].dogru	=	2
Sorular[	82	].dogru	=	3
Sorular[	83	].dogru	=	4
Sorular[	84	].dogru	=	1
Sorular[	85	].dogru	=	3
Sorular[	86	].dogru	=	4
Sorular[	87	].dogru	=	4
Sorular[	88	].dogru	=	4
Sorular[	89	].dogru	=	2
Sorular[	90	].dogru	=	3

Sorular[	91	].dogru	=	4
Sorular[	92	].dogru	=	3
Sorular[	93	].dogru	=	4
Sorular[	94	].dogru	=	4
Sorular[	95	].dogru	=	3
Sorular[	96	].dogru	=	3
Sorular[	97	].dogru	=	4
Sorular[	98	].dogru	=	4
Sorular[	99	].dogru	=	1
Sorular[	100	].dogru	=	4

Sorular[	101	].dogru	=	3
Sorular[	102	].dogru	=	2
Sorular[	103	].dogru	=	3
Sorular[	104	].dogru	=	3
Sorular[	105	].dogru	=	2
