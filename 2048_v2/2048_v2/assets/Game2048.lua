Game2048 = gideros.class(Sprite)


function Game2048:init(selected)
	self:loadData(selected)
	self:loadBK()
	self:createGrids()
	self:randomCell()	

	self:addEventListener(Event.MOUSE_DOWN, self.onMouseDown, self)
	self:addEventListener(Event.MOUSE_UP, self.onMouseUp, self)
	
	self.mouseOn = {}
	--self:draw()
end


function Game2048:loadData(selected)
	back = imageList[selected].bk
	iList = {0,2,4,8,16,32,64,128,256,512,1024,2048}
	for i = 1, #iList do
		glist[iList[i]].image = imageList[selected][iList[i]]
	end 

end 


function Game2048:createGrids()
	self.superGrid = {}
	self.superGridCopy = {} 
	
	for i=1,4 do
		self.superGrid[i] = {}
		self.superGridCopy[i] = {}
		for j = 1,4 do
			self.superGrid[i][j] = { val = 0, oldval = 0, plus = false, oldI = i, oldJ = j, random = false }
			self.superGridCopy[i][j] = {}
		end 	
	end 	
	
	self.pos = {}
	local list = {20, 160, 160+140, 160+140*2} 

	for i=1,4 do
		self.pos[i] = {}
		for j = 1,4 do
			self.pos[i][j] = {x = list[i], y = list[j]}
		end 	
	end 
	
	self.tempImages = {}
	self.availableCells = {}	
	
	
end 

function Game2048:loadBK()



	self.qSprite = Sprite.new()
	self.qSprite.bk = Bitmap.new(TextureRegion.new(Texture.new(back, true )))
	self:addChild(self.qSprite.bk)

	local xx = (X_HIGH - self.qSprite.bk:getWidth())  / 2
	local yy = (Y_HIGH - self.qSprite.bk:getHeight())  / 2
	self.qSprite.bk:setPosition(xx,yy)

	self.geri = Bitmap.new(TextureRegion.new(Texture.new(geri, true )))
	self:addChild(self.geri)
	self.menu = Bitmap.new(TextureRegion.new(Texture.new(menu, true )))
	self:addChild(self.menu)



	self.geri:setPosition(400, 20+40+10)
	self.geri:setAlpha(0)
	self.menu:setAlpha(0)

	self.menu:setPosition(40, 20+40+10)
	
	self.xx = xx
	self.yy = yy
	
	self.qSprite:setPosition(30,400)
	self:addChild(self.qSprite)
end 


function Game2048:onMouseDown(event)
	self.mouseOn.on = true
	self.mouseOn.startX = event.x
	self.mouseOn.startY = event.y 

end 


function Game2048:onMouseUp(event)
	if self.geri:hitTestPoint(event.x, event.y) == true then 
					ev= Event.new("hebe")
					ev.page = 2
					ev.selected = i
					self.manager:gotoPage(ev)	
	
	elseif self.menu:hitTestPoint(event.x, event.y) == true then 
					ev= Event.new("hebe")
					ev.page = 4
					ev.selected = i
					self.manager:gotoPage(ev)	
	
	else 

		if self.mouseOn.on then 
			local deltaX = event.x - self.mouseOn.startX
			local deltaY = event.y - self.mouseOn.startY
			self:setSwipe(deltaX, deltaY)
			self.mouseOn.on = false
		end 
	end 	
end 

function Game2048:copyOldGrid()
	for i=1,4 do
		self.superGridCopy[i] = {} 
		for j = 1,4 do
			self.superGridCopy[i][j] ={ }
			self.superGridCopy[i][j].image  = self.superGrid[i][j].image
		end 
	end 	

end 

function Game2048:checkChange()
	
	for j = 1,4 do
		for i = 1,4 do
 
		--	print("*--***", i,j, self.superGrid[i][j].val , self.superGrid[i][j].oldVal, self.superGrid[i][j].oldI, self.superGrid[i][j].oldJ )
			
			if self.superGrid[i][j].val ~= self.superGrid[i][j].oldVal then 
				return false
			end 	
		end
	end 	
	return true
end 

function Game2048:randomCell()
	self.availableCells = {}
	
	for i =1,4 do
		for j = 1,4 do
			self.superGrid[i][j].random = false 
			if self.superGrid[i][j].val == 0 then 
				self.availableCells[#self.availableCells + 1] = {ii = i, jj = j }
			end 	
		end 
	end 	
		

	
	if #self.availableCells > 0 then 
	--	print("random sayisi", #self.availableCells)
		local  p = math.random(1,#self.availableCells)
		local  i = self.availableCells[p].ii
		local  j = self.availableCells[p].jj
		
		self.superGrid[i][j].val = 2
		self.superGrid[i][j].oldVal = 0
		self.superGrid[i][j].oldI = i
		self.superGrid[i][j].oldJ = j
		self.superGrid[i][j].plus = false 
		self.superGrid[i][j].random = true 
		
		self.randomCellGrid = {i = i, j = j}		
	end 
	self.availableCells = {}
end 

function Game2048:cleanAll()
	for i = 1,  #self.tempImages do
		self.qSprite:removeChild(self.tempImages[i])
	end 
	self.tempImages = {}

end 

function Game2048:makeChanges()

	if not self:checkChange() then 
		self:randomCell()
	

	self:cleanAll()
	self.tweenlist = {}
	self.tempImages = {}
	
	self.tweenPlan ={}
	
		
		print("-----------------------------------------------------------------------------------------------------------------")
	for i=1,4 do
		print(self.superGrid[1][i].val, "  ",  self.superGrid[2][i].val , "  ", self.superGrid[3][i].val, "   ", self.superGrid[4][i].val)
	end 	
		print("-----------------------------------------------------------------------------------------------------------------")
		
	end 	

	for i=1,4 do
		for j = 1,4 do
			if self.superGrid[i][j].plus == true and self.superGrid[i][j].val > 0		then 
				print("00000000000000000000000000000000000000000000000", self.superGrid[i][j].val, i, j )
				self.superGrid[i][j].image = Bitmap.new(TextureRegion.new(Texture.new(glist[self.superGrid[i][j].val].image, true )))
				self.superGrid[i][j].image:setAnchorPoint(0.5,0.5)
				local w = self.superGrid[i][j].image:getWidth()
				self.tempImages[#self.tempImages +1] = self.superGrid[i][j].image
				self.tempImages[#self.tempImages ].data = {ii = i, jj = j}
				self.superGrid[i][j].image:setPosition(self.pos[i][j].x + self.xx +w /2, self.pos[i][j].y + self.yy + w/2)
				
				local oldI = self.superGrid[i][j].oldI
				local oldJ = self.superGrid[i][j].oldJ
				
				local im = self.superGridCopy[oldI][oldJ].image
				self.qSprite:addChild(im)
				self.qSprite:addChild(self.superGrid[i][j].image)
	
				self.tweenlist[#self.tweenlist + 1]  = GTween.new(im, 0.2, {x = self.pos[i][j].x + self.xx +w /2, y = self.pos[i][j].y + self.yy + w/2}, { ease = easing.linear, delay = 0, repeatCount = 1, reflect = true } )
				self.tweenlist[#self.tweenlist]:addEventListener("complete", 
				function () 
						self.qSprite:removeChild(im)
					end, self)
				self.tweenlist[#self.tweenlist].dispatchEvents = true	
	

				--self.tweenPlan[#self.tweenPlan + 1] = { im = self.superGrid[i][j].image

				self.tweenlist[#self.tweenlist + 1]  = GTween.new(self.superGrid[i][j].image, 0.2, {scaleX = 1.1, scaleY = 1.1}, { ease = easing.outBack, delay =0.2, repeatCount = 2, reflect = true } )
			end
		end 
	end 	
	
	for i=1,4 do
		for j = 1,4 do	
		--	print("normal", i,j, "random ", self.randomCellGrid.i, self.randomCellGrid.j)
			if (i == self.randomCellGrid.i) and (j == self.randomCellGrid.j) then 
				--print(" -------------------------- >>>>> ATLADI", i,j, "random ", self.randomCellGrid.i, self.randomCellGrid.j)
			else 	
				if self.superGrid[i][j].val ~= 0 and  self.superGrid[i][j].plus == false then
					--print("------**-*-*-*-*-*-*-*-", self.superGrid[i][j].val)

						self.superGrid[i][j].image = Bitmap.new(TextureRegion.new(Texture.new(glist[self.superGrid[i][j].val].image, true )))
						self.superGrid[i][j].image:setAnchorPoint(0.5,0.5)
						local w = self.superGrid[i][j].image:getWidth()
						self.tempImages[#self.tempImages +1] = self.superGrid[i][j].image
						self.tempImages[#self.tempImages ].data = {ii = i, jj = j}
						local ii = self.superGrid[i][j].oldI
						local jj = self.superGrid[i][j].oldJ
						
						self.superGrid[i][j].image:setPosition(self.pos[ii][jj].x + self.xx+w/2 , self.pos[ii][jj].y + self.yy+w/2)
						self.qSprite:addChild(self.superGrid[i][j].image)	
						

						self.tweenlist[#self.tweenlist + 1] = GTween.new(self.superGrid[i][j].image, 0.3, {x =self.pos[i][j].x + self.xx +w/2 , y = self.pos[i][j].y + self.yy+w/2}, { ease = easing.linear, delay = 0, repeatCount = 1, reflect = false } )
					
				
				end 
			end 
		end 		
		
	end

	if self.randomCellGrid ~= nil then 
			i = self.randomCellGrid.i 
			j = self.randomCellGrid.j 
			print("current!" , self.superGrid[i][j].val, glist[self.superGrid[i][j].val].image)
			self.superGrid[i][j].image = Bitmap.new(TextureRegion.new(Texture.new(glist[self.superGrid[i][j].val].image, true )))
			self.superGrid[i][j].image:setAnchorPoint(0.5,0.5)
			local w = self.superGrid[i][j].image:getWidth()
			self.tempImages[#self.tempImages +1] = self.superGrid[i][j].image
			self.tempImages[#self.tempImages ].data = {ii = i, jj = j}
			local ii = self.superGrid[i][j].oldI
			local jj = self.superGrid[i][j].oldJ
			
			self.superGrid[i][j].image:setPosition(self.pos[ii][jj].x + self.xx+w/2 , self.pos[ii][jj].y + self.yy+w/2)
			self.qSprite:addChild(self.superGrid[i][j].image)	
			self.superGrid[i][j].image:setAlpha(0)
			
		--	self.superGrid[i][j].image:setColorTransform(1,0,1,1)
			self.superGrid[i][j].image:setScale(0.5,0.5)
			if self.tweenlist then 
				self.tweenlist[#self.tweenlist + 1] = GTween.new(self.superGrid[i][j].image, 0.3, {alpha = 1, scaleX = 1, scaleY = 1}, { ease = easing.linear, delay = 0.5, repeatCount = 1, reflect = false } )
			end 	
	end 	



end 


function Game2048:setSwipe(X, Y)
	self:backUp()
	self:copyOldGrid()
	
	if math.abs(X) >  math.abs(Y) then 
		if X > 0  then self:swipeRight()
		else self:swipeLeft()
		end 
	else
		if Y > 0  then self:swipeDown()
		else self:swipeUp()
		end 	
	
	end 

end 



function Game2048:backUp()
	for j = 1,4 do
		for i = 1,4 do
			self.superGrid[i][j].oldVal = self.superGrid[i][j].val 
			self.superGrid[i][j].oldI  = i
			self.superGrid[i][j].oldJ  = j
			self.superGrid[i][j].plus = false 
		end
	end 
end 


function Game2048:swipeRight()
	self:backUp()
	for j = 1,4 do
		for p = 1,3 do
			for i = 4,2,-1 do 
				local t = self.superGrid[i][j].val
				if t == 0 then 
					self.superGrid[i][j].val = self.superGrid[i-1][j].val
					self.superGrid[i][j].oldI = self.superGrid[i-1][j].oldI
					self.superGrid[i][j].oldJ = self.superGrid[i-1][j].oldJ
					self.superGrid[i][j].image = self.superGrid[i-1][j].image
			
					self.superGrid[i-1][j].val = 0
				end 	
			end 
		end 
	
		for i = 4,2,-1 do 
			local t = self.superGrid[i][j].val

			if t == self.superGrid[i-1][j].val and t ~= 0 then 
				--self.superGrid[i][j].oldval = self.superGrid[i][j].val	
				self.superGrid[i][j].val = self.superGrid[i][j].val * 2
				self.superGrid[i][j].plus = true
				self.superGrid[i][j].oldI = self.superGrid[i-1][j].oldI
				self.superGrid[i][j].oldJ = self.superGrid[i-1][j].oldJ
				self.superGrid[i-1][j].val = 0	
		
			end 	
		end 		
		
		for p = 1,3 do
			for i = 4,2,-1 do 
				local t = self.superGrid[i][j].val
				if t == 0 then 
					self.superGrid[i][j].val = self.superGrid[i-1][j].val
					self.superGrid[i][j].oldI = self.superGrid[i-1][j].oldI
					self.superGrid[i][j].oldJ = self.superGrid[i-1][j].oldJ		
					self.superGrid[i][j].image = self.superGrid[i-1][j].image		
					self.superGrid[i-1][j].val = 0
				end 	
			end 
		end 
		

	end 	
	self:makeChanges()
end 

function Game2048:swipeLeft()
	self:backUp()
	for j = 1,4 do
		for p = 1,3 do
			for i = 1,3 do 
				local t = self.superGrid[i][j].val
				if t == 0 then 
					self.superGrid[i][j].val = self.superGrid[i+1][j].val
					self.superGrid[i][j].oldI = self.superGrid[i+1][j].oldI
					self.superGrid[i][j].oldJ = self.superGrid[i+1][j].oldJ
					self.superGrid[i][j].image = self.superGrid[i+1][j].image
					
					self.superGrid[i+1][j].val = 0
				end 	
			end 
		end 
	
		for i = 1,3 do 
			local t = self.superGrid[i][j].val

			if t == self.superGrid[i+1][j].val and t ~= 0 then 
				--self.superGrid[i][j].oldval = self.superGrid[i][j].val	
				self.superGrid[i][j].val = self.superGrid[i][j].val * 2
				self.superGrid[i][j].plus = true
					self.superGrid[i][j].oldI = self.superGrid[i+1][j].oldI
					self.superGrid[i][j].oldJ = self.superGrid[i+1][j].oldJ				
				self.superGrid[i+1][j].val = 0	
			end 	
		end 		
		
		for p = 1,3 do
			for i = 1,3 do 
				local t = self.superGrid[i][j].val
				if t == 0 then 
					self.superGrid[i][j].val = self.superGrid[i+1][j].val
					self.superGrid[i][j].oldI = self.superGrid[i+1][j].oldI
					self.superGrid[i][j].oldJ = self.superGrid[i+1][j].oldJ		
					self.superGrid[i][j].image = self.superGrid[i+1][j].image		
					self.superGrid[i+1][j].val = 0
				end 	
			end 
		end 
		

	end 	
	self:makeChanges()
end 

function Game2048:swipeDown()
	self:backUp()
	for i = 1,4 do
		for p = 1,3 do
			for j = 4,2,-1 do 
				local t = self.superGrid[i][j].val
				if t == 0 then 
					self.superGrid[i][j].val = self.superGrid[i][j-1].val
					self.superGrid[i][j].oldI = self.superGrid[i][j-1].oldI
					self.superGrid[i][j].oldJ = self.superGrid[i][j-1].oldJ
					self.superGrid[i][j].image = self.superGrid[i][j-1].image
					
					self.superGrid[i][j-1].val = 0
				end 	
			end 
		end 
	
		for j = 4,2,-1 do 
			local t = self.superGrid[i][j].val

			if t == self.superGrid[i][j-1].val and t ~= 0 then 
				--self.superGrid[i][j].oldval = self.superGrid[i][j].val	
				self.superGrid[i][j].val = self.superGrid[i][j].val * 2
				self.superGrid[i][j].plus = true
				self.superGrid[i][j].oldI = self.superGrid[i][j-1].oldI
				self.superGrid[i][j].oldJ = self.superGrid[i][j-1].oldJ				
				self.superGrid[i][j-1].val = 0	
				
			end 	
		end 		
		
		for p = 1,3 do
			for j = 4,2,-1 do 
				local t = self.superGrid[i][j].val
				if t == 0 then 
					self.superGrid[i][j].val = self.superGrid[i][j-1].val
					self.superGrid[i][j].oldI = self.superGrid[i][j-1].oldI
					self.superGrid[i][j].oldJ = self.superGrid[i][j-1].oldJ		
					self.superGrid[i][j].image = self.superGrid[i][j-1].image		
					self.superGrid[i][j-1].val = 0
				end 	
			end 
		end 
	end 
		
	self:makeChanges()
end 
function Game2048:swipeUp()
	self:backUp()
	for i = 1,4 do

		for p = 1,3 do
			for j = 1,3 do 
				local t = self.superGrid[i][j].val
				if t == 0 then 
					self.superGrid[i][j].val = self.superGrid[i][j+1].val
					self.superGrid[i][j].oldI = self.superGrid[i][j+1].oldI
					self.superGrid[i][j].oldJ = self.superGrid[i][j+1].oldJ
					self.superGrid[i][j].image = self.superGrid[i][j+1].image
					
					self.superGrid[i][j+1].val = 0
				end 	
			end 
		end 
	
		for j = 1,3 do 
			local t = self.superGrid[i][j].val

			if t == self.superGrid[i][j+1].val and t ~= 0 then 
				--self.superGrid[i][j].oldval = self.superGrid[i][j].val	
				self.superGrid[i][j].val = self.superGrid[i][j].val * 2
				self.superGrid[i][j].plus = true
				self.superGrid[i][j].oldI = self.superGrid[i][j+1].oldI
				self.superGrid[i][j].oldJ = self.superGrid[i][j+1].oldJ				
				self.superGrid[i][j+1].val = 0	
				
			end 	
		end 		
		
		for p = 1,3 do
			for j = 1,3 do 
				local t = self.superGrid[i][j].val
				if t == 0 then 
					self.superGrid[i][j].val = self.superGrid[i][j+1].val
					self.superGrid[i][j].oldI = self.superGrid[i][j+1].oldI
					self.superGrid[i][j].oldJ = self.superGrid[i][j+1].oldJ		
					self.superGrid[i][j].image = self.superGrid[i][j+1].image		
					self.superGrid[i][j+1].val = 0
				end 	
			end 
		end 
		

	end 	
	self:makeChanges()
end 
