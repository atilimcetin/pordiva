
Cover = gideros.class(Sprite)


function Cover:init()

	self.bk = Bitmap.new(TextureRegion.new(Texture.new("gfx/bas.png", true )))
	self:addChild(self.bk)

	self.buttons  = {}
	
	self.buttons[1] = Bitmap.new(TextureRegion.new(Texture.new("gfx/Efsane.png", true )))
	self.buttons[2] = Bitmap.new(TextureRegion.new(Texture.new("gfx/forma.png", true )))
	self.buttons[3] = Bitmap.new(TextureRegion.new(Texture.new("gfx/ilk11.png", true )))
	self.buttons[4] = Bitmap.new(TextureRegion.new(Texture.new("gfx/klasik.png", true )))

	for i = 1,4 do
		self.buttons[i]:setAnchorPoint(0.5, 0.5)
		self.bk:addChild(self.buttons[i])
		self.buttons[i].x = 320
		self.buttons[i]:setScale(0.2)
		self.buttons[i]:setAlpha(0)
	end 
	local st = 340
	local d = 175
		
	self.buttons[1].y = st
	self.buttons[2].y = self.buttons[1].y + d
 	self.buttons[3].y = self.buttons[2].y + d
	self.buttons[4].y = self.buttons[3].y + d

	for i=1,4 do
		self.buttons[i]:setPosition(self.buttons[i].x, self.buttons[i].y )	
	
	end
	


	self.tt = {}
	for i=1,4 do
		self.tt[i] = GTween.new(self.buttons[i], 0.3, {alpha = 1, scaleX = 1, scaleY = 1}, { ease = easing.outBack, delay = i * 0.1 + 0.2, repeatCount = 1, reflect = true } )
		self.tt[i]:addEventListener("complete", function()  
				self.ss = "sound/hit.wav"
				self.ss2 = Sound.new(self.ss)
			--	self.soundchannel = self.ss2:play()
			end ,self)
		self.tt[i].dispatchEvents = true 	
	end 

	self.tt2 = {}
--	self:addEventListener(Event.MOUSE_DOWN, self.onMouseDown, self)
	self:addEventListener(Event.MOUSE_UP, self.onMouseUp, self)
	
	
end 


function Cover:onMouseUp(event)
	for i=1,4 do
		if self.buttons[i]:hitTestPoint(event.x,event.y) == true then 
			self.tt2[i] = GTween.new(self.buttons[i], 0.5, {alpha = 1, scaleX = 1.1, scaleY = 1.1}, { ease = easing.outBack, delay = 0, repeatCount = 1, reflect = true } )
			self.tt2[i]:addEventListener("complete", function()  
					ev= Event.new("hebe")
					ev.page = 3
					ev.selected = i
					self.manager:gotoPage(ev)	
					
				end ,self)
			self.tt2[i].dispatchEvents = true 	
		end 
	end 
end 





