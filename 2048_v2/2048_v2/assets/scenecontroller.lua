sceneController =  gideros.class(Sprite)
startPage = 1
endPage = 4

local pages = {}
-- create a list of pages, to set their order of apperance. 
pages[0] = "Page3"
pages[1] = "Start"
pages[2] = "Cover"
pages[3] = "Game2048"
pages[4] = "Scor"


function sceneController:init()
	self.scenes = SceneManager.new({ --scenes are defined
	["Page0"] = Page0, 
	["Page1"] = Page1, 
	["Cover"] = Cover, 	
	["Game2048"] = Game2048,
	["Scor"] = Scor,
	["Start"] = Start,

})


	self:addChild(self.scenes)
	-- add the first page
	print("dd the first page", pages[startPage])
	self.scenes:changeScene(pages[startPage])
	self.scenes.scene1.manager = self
	



	self.pageNo = startPage

end


function sceneController:leftClick()
	print("left click", self.pageNo)
	-- previous index
	if self.scenes.tweening == false then 
		self.pageNo = self.pageNo - 1
		if self.pageNo < 0 then self.pageNo = 0 end	
		print(self.pageNo)
		self:checkVisible()
		
		-- give next scene, the duration, the effect, and the easing mode. you can play with them to see how they look.
		self.scenes:changeScene(pages[self.pageNo],0.5, flipFunctionForward , easeFunctionForward)
	end 	
end 

function sceneController:checkVisible()
	self.right:setVisible(true)
	self.left:setVisible(true)
	
	if (self.pageNo == endPage) then 
		self.right:setVisible(false)
	end 		
	if (self.pageNo == 0) then 
		self.left:setVisible(false)
	end 
			
end 

function sceneController:rightClick()
--	if self.changeSceneTransitionCompleteLeft then 
	if self.scenes.tweening == false then 
		self.pageNo = self.pageNo + 1
		print(self.pageNo)
		self:checkVisible()
		if self.pageNo <= endPage then 
			self.scenes:changeScene(pages[self.pageNo],0.5, flipFunctionBackward, easeFunctionBackward) 
		end	
	end	
end 
	
function sceneController:gotoPage(event)	 
	print("EVENT PAGE ", event.page)
	if  event.page <= endPage  then
		self.pageNo = event.page
		if event.level == nil then event.level = 4 end 
		self.scenes:changeScene(pages[event.page],1, SceneManager.crossfade, easeFunctionBackward, event.selected) 
		self.scenes.scene1.manager = self
		self.scenes.scene2.manager = self
	end 	
end 
